--
--  Copyright (C) 2020 Tobias Brunner <tobias@codelabs.ch>
--  Copyright (C) 2013 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions
--  are met:
--  1. Redistributions of source code must retain the above copyright
--     notice, this list of conditions and the following disclaimer.
--  2. Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
--  3. Neither the name of the University nor the names of its contributors
--     may be used to endorse or promote products derived from this software
--     without specific prior written permission.
--
--  THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
--  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
--  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
--  ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
--  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
--  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
--  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
--  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
--  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
--  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
--  SUCH DAMAGE.
--

with Ada.Streams;

with Anet.Sockets.Unix;

with Tkmrpc.Request.Convert;
with Tkmrpc.Response.Convert;
with Tkmrpc.Types;

pragma Elaborate_All (Anet.Sockets.Unix);

package body Tkmrpc.Transport.Client
is
   use type Response.Data_Type;

   --  This type handles requests and responses by multiple senders.
   protected type Sender_Receiver
   is

      --  Connect socket to given address.
      procedure Connect (Address : String);

      --  Delivers a response to waiting senders.
      entry Deliver_Response (Res_Data : Response.Data_Type);

      --  Stop receiving responses.
      procedure Finalize;

      --  Send request data and return received response.
      entry Send_Receive
        (Req_Data : in out Request.Data_Type;
         Res_Data :    out Response.Data_Type);

      --  Entry the receiver task waits on.
      entry Wait_For_Finalize;

   private

      --  Receive response data for the given request.
      entry Receive
        (Req_Data : in out Request.Data_Type;
         Res_Data :    out Response.Data_Type);

      --  The request ID assigned to the next request sent.
      Next_Id   : Types.Request_Id_Type;

      --  Received response.
      Received  : Response.Data_Type := Response.Null_Data;

      --  Whether the socket was finalized.
      Finalized : Boolean;

   end Sender_Receiver;

   --  Task that receives responses.
   task Receiver
   is
      entry Start;
   end Receiver;

   Socket : Anet.Sockets.Unix.TCP_Socket_Type;
   Sender : Sender_Receiver;

   -------------------------------------------------------------------------

   procedure Connect (Address : String)
   is
   begin
      Sender.Connect (Address => Address);
      Receiver.Start;
   end Connect;

   -------------------------------------------------------------------------

   procedure Disconnect
   is
   begin
      Sender.Finalize;
   end Disconnect;

   -------------------------------------------------------------------------

   procedure Send_Receive
     (Req_Data :     Request.Data_Type;
      Res_Data : out Response.Data_Type)
   is
      Req : Request.Data_Type := Req_Data;
   begin
      Sender.Send_Receive (Req_Data => Req,
                           Res_Data => Res_Data);
   end Send_Receive;

   -------------------------------------------------------------------------

   protected body Sender_Receiver
   is

      ----------------------------------------------------------------------

      procedure Connect (Address : String)
      is
      begin
         Next_Id   := 0;
         Finalized := False;
         Received  := Response.Null_Data;

         Socket.Init;
         Socket.Connect (Path => Anet.Sockets.Unix.Path_Type (Address));
      end Connect;

      ----------------------------------------------------------------------

      entry Deliver_Response (Res_Data : Response.Data_Type)
        when Received = Response.Null_Data
      is
      begin
         Received := Res_Data;
      end Deliver_Response;

      ----------------------------------------------------------------------

      procedure Finalize
      is
      begin
         Finalized := True;
      end Finalize;

      ----------------------------------------------------------------------

      entry Send_Receive
        (Req_Data : in out Request.Data_Type;
         Res_Data :    out Response.Data_Type)
        when True
      is
         use type Types.Request_Id_Type;
      begin
         Req_Data.Header.Request_Id := Next_Id;
         Next_Id := Next_Id + 1;

         Socket.Send (Item => Request.Convert.To_Stream (S => Req_Data));

         requeue Receive;
      end Send_Receive;

      ----------------------------------------------------------------------

      entry Receive
        (Req_Data : in out Request.Data_Type;
         Res_Data :    out Response.Data_Type)
        when Received /= Response.Null_Data
      is
         use type Types.Request_Id_Type;
      begin
         if Req_Data.Header.Request_Id /= Received.Header.Request_Id then
            requeue Receive;
         end if;

         Res_Data := Received;
         Received := Response.Null_Data;
      end Receive;

      ----------------------------------------------------------------------

      entry Wait_For_Finalize when Finalized
      is
      begin
         null;
      end Wait_For_Finalize;

   end Sender_Receiver;

   -------------------------------------------------------------------------

   task body Receiver
   is
      use type Ada.Streams.Stream_Element_Offset;

      Buffer : Ada.Streams.Stream_Element_Array
        (1 .. Response.Response_Size);
      Last   : Ada.Streams.Stream_Element_Offset;
   begin
      loop
         select
            accept Start;
         or
            terminate;
         end select;

         Receive_Loop :
         loop
            select
               Sender.Wait_For_Finalize;
               exit Receive_Loop;
            then abort
               Socket.Receive
                 (Item => Buffer,
                  Last => Last);
            end select;

            --  Stop processing on this socket if it gets closed.
            exit Receive_Loop when Last = 0;

            Sender.Deliver_Response
              (Res_Data => Response.Convert.From_Stream (S => Buffer));

         end loop Receive_Loop;
      end loop;
   end Receiver;

end Tkmrpc.Transport.Client;
