#pragma once

#include <stdint.h>

typedef unsigned char byte_t;

typedef uint64_t operation_type;

typedef uint64_t request_id_type;

typedef uint64_t result_type;

typedef uint64_t version_type;

typedef uint64_t active_requests_type;

typedef uint64_t authag_id_type;

typedef uint64_t cag_id_type;

typedef uint64_t li_id_type;

typedef uint64_t ri_id_type;

typedef uint64_t iag_id_type;

typedef uint64_t eag_id_type;

typedef uint64_t keag_id_type;

typedef uint32_t sp_id_type;

typedef uint64_t authp_id_type;

typedef uint64_t kep_id_type;

typedef uint64_t autha_id_type;

typedef uint64_t ca_id_type;

typedef uint64_t lc_id_type;

typedef uint64_t ia_id_type;

typedef uint64_t ea_id_type;

typedef uint64_t kea_id_type;

typedef uint64_t nc_id_type;

typedef uint64_t ke_id_type;

typedef uint64_t cc_id_type;

typedef uint64_t ae_id_type;

typedef uint64_t isa_id_type;

typedef uint64_t esa_id_type;

typedef uint64_t esp_enc_id_type;

typedef uint64_t esp_dec_id_type;

typedef uint64_t esp_map_id_type;

typedef uint64_t abs_time_type;

typedef uint64_t rel_time_type;

typedef uint64_t duration_type;

typedef uint64_t counter_type;

typedef uint64_t pfs_flag_type;

typedef uint64_t cc_time_flag_type;

typedef uint8_t expiry_flag_type;

typedef uint64_t auth_algorithm_type;

typedef uint16_t ke_algorithm_type;

typedef uint16_t prf_algorithm_type;

typedef uint16_t int_algorithm_type;

typedef uint16_t enc_algorithm_type;

typedef uint64_t key_length_bits_type;

typedef uint64_t block_length_bits_type;

typedef uint32_t protocol_type;

typedef uint64_t init_type;

typedef uint64_t ike_spi_type;

typedef uint32_t esp_spi_type;

typedef uint64_t esa_flags_type;

typedef uint64_t nonce_length_type;

typedef struct init_message_type init_message_type;

struct __attribute__((__packed__)) init_message_type {
    uint32_t size;
    uint8_t data[1500];
};

typedef struct certificate_type certificate_type;

struct __attribute__((__packed__)) certificate_type {
    uint32_t size;
    uint8_t data[2000];
};

typedef struct nonce_type nonce_type;

struct __attribute__((__packed__)) nonce_type {
    uint32_t size;
    uint8_t data[256];
};

typedef struct ke_pubvalue_type ke_pubvalue_type;

struct __attribute__((__packed__)) ke_pubvalue_type {
    uint32_t size;
    uint8_t data[512];
};

typedef struct ke_priv_type ke_priv_type;

struct __attribute__((__packed__)) ke_priv_type {
    uint32_t size;
    uint8_t data[512];
};

typedef struct ke_key_type ke_key_type;

struct __attribute__((__packed__)) ke_key_type {
    uint32_t size;
    uint8_t data[512];
};

typedef struct key_type key_type;

struct __attribute__((__packed__)) key_type {
    uint32_t size;
    uint8_t data[64];
};

typedef struct intauth_type intauth_type;

struct __attribute__((__packed__)) intauth_type {
    uint32_t size;
    uint8_t data[64];
};

typedef uint32_t intauth_count_type;

typedef struct identity_type identity_type;

struct __attribute__((__packed__)) identity_type {
    uint32_t size;
    uint8_t data[64];
};

typedef struct signature_type signature_type;

struct __attribute__((__packed__)) signature_type {
    uint32_t size;
    uint8_t data[384];
};

typedef struct auth_parameter_type auth_parameter_type;

struct __attribute__((__packed__)) auth_parameter_type {
    uint32_t size;
    uint8_t data[1024];
};

typedef struct ke_parameter_type ke_parameter_type;

struct __attribute__((__packed__)) ke_parameter_type {
    uint32_t size;
    uint8_t data[1024];
};

typedef uint16_t aad_len_type;

typedef uint16_t iv_len_type;

typedef uint16_t icv_len_type;

typedef uint16_t block_len_type;

typedef struct aad_plain_type aad_plain_type;

struct __attribute__((__packed__)) aad_plain_type {
    uint32_t size;
    uint8_t data[2016];
};

typedef struct iv_encrypted_icv_type iv_encrypted_icv_type;

struct __attribute__((__packed__)) iv_encrypted_icv_type {
    uint32_t size;
    uint8_t data[2019];
};

typedef struct aad_iv_encrypted_icv_type aad_iv_encrypted_icv_type;

struct __attribute__((__packed__)) aad_iv_encrypted_icv_type {
    uint32_t size;
    uint8_t data[2016];
};

typedef struct decrypted_type decrypted_type;

struct __attribute__((__packed__)) decrypted_type {
    uint32_t size;
    uint8_t data[2016];
};

typedef uint64_t blob_id_type;

typedef uint32_t blob_length_type;

typedef uint32_t blob_offset_type;

typedef struct blob_out_bytes_type blob_out_bytes_type;

struct __attribute__((__packed__)) blob_out_bytes_type {
    uint8_t data[2024];
};

typedef struct blob_in_bytes_type blob_in_bytes_type;

struct __attribute__((__packed__)) blob_in_bytes_type {
    uint32_t size;
    uint8_t data[2016];
};

typedef uint8_t inbound_flag_type;

typedef struct ke_ids_type ke_ids_type;

struct __attribute__((__packed__)) ke_ids_type {
    uint32_t size;
    uint64_t data[8];
};

