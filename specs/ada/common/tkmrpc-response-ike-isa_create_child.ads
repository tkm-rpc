with Tkmrpc.Types;
with Tkmrpc.Operations.Ike;

package Tkmrpc.Response.Ike.Isa_Create_Child is

   Data_Size : constant := 6;

   type Data_Type is record
      Block_Len : Types.Block_Len_Type;
      Icv_Len   : Types.Icv_Len_Type;
      Iv_Len    : Types.Iv_Len_Type;
   end record;

   for Data_Type use record
      Block_Len at 0 range 0 .. (2 * 8) - 1;
      Icv_Len   at 2 range 0 .. (2 * 8) - 1;
      Iv_Len    at 4 range 0 .. (2 * 8) - 1;
   end record;
   for Data_Type'Size use Data_Size * 8;

   Padding_Size : constant := Response.Body_Size - Data_Size;
   subtype Padding_Type is Types.Byte_Sequence (1 .. Padding_Size);

   type Response_Type is record
      Header  : Response.Header_Type;
      Data    : Data_Type;
      Padding : Padding_Type;
   end record;

   for Response_Type use record
      Header at 0 range 0 .. (Response.Header_Size * 8) - 1;
      Data   at Response.Header_Size range 0 .. (Data_Size * 8) - 1;
      Padding at Response.Header_Size + Data_Size range 0 ..
          (Padding_Size * 8) - 1;
   end record;
   for Response_Type'Size use Response.Response_Size * 8;

   Null_Response : constant Response_Type :=
     Response_Type'
       (Header =>
          Response.Header_Type'
            (Operation => Operations.Ike.Isa_Create_Child,
             Result    => Results.Invalid_Operation, Request_Id => 0),
        Data =>
          Data_Type'
            (Block_Len => Types.Block_Len_Type'First,
             Icv_Len   => Types.Icv_Len_Type'First,
             Iv_Len    => Types.Iv_Len_Type'First),
        Padding => Padding_Type'(others => 0));

end Tkmrpc.Response.Ike.Isa_Create_Child;
