with Ada.Unchecked_Conversion;

package Tkmrpc.Response.Ike.Ke_Set.Convert is

   function To_Response is new Ada.Unchecked_Conversion
     (Source => Ke_Set.Response_Type, Target => Response.Data_Type);

   function From_Response is new Ada.Unchecked_Conversion
     (Source => Response.Data_Type, Target => Ke_Set.Response_Type);

end Tkmrpc.Response.Ike.Ke_Set.Convert;
