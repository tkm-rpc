
package Tkmrpc.Operations.Ike is

   Tkm_Version : constant Operations.Operation_Type := 16#0000#;

   Tkm_Limits : constant Operations.Operation_Type := 16#0001#;

   Tkm_Reset : constant Operations.Operation_Type := 16#0002#;

   Nc_Reset : constant Operations.Operation_Type := 16#0100#;

   Nc_Create : constant Operations.Operation_Type := 16#0101#;

   Ke_Reset : constant Operations.Operation_Type := 16#0200#;

   Ke_Get : constant Operations.Operation_Type := 16#0201#;

   Ke_Set : constant Operations.Operation_Type := 16#0202#;

   Cc_Reset : constant Operations.Operation_Type := 16#0300#;

   Cc_Check_Chain : constant Operations.Operation_Type := 16#0301#;

   Cc_Add_Certificate : constant Operations.Operation_Type := 16#0302#;

   Cc_Check_Ca : constant Operations.Operation_Type := 16#0303#;

   Ae_Reset : constant Operations.Operation_Type := 16#0800#;

   Isa_Reset : constant Operations.Operation_Type := 16#0900#;

   Isa_Create : constant Operations.Operation_Type := 16#0901#;

   Isa_Sign : constant Operations.Operation_Type := 16#0902#;

   Isa_Auth : constant Operations.Operation_Type := 16#0903#;

   Isa_Create_Child : constant Operations.Operation_Type := 16#0904#;

   Isa_Skip_Create_First : constant Operations.Operation_Type := 16#0905#;

   Isa_Encrypt : constant Operations.Operation_Type := 16#0906#;

   Isa_Decrypt : constant Operations.Operation_Type := 16#0907#;

   Isa_Update : constant Operations.Operation_Type := 16#0908#;

   Isa_Int_Auth : constant Operations.Operation_Type := 16#0909#;

   Esa_Reset : constant Operations.Operation_Type := 16#0A00#;

   Esa_Create : constant Operations.Operation_Type := 16#0A01#;

   Esa_Create_No_Pfs : constant Operations.Operation_Type := 16#0A02#;

   Esa_Create_First : constant Operations.Operation_Type := 16#0A03#;

   Esa_Select : constant Operations.Operation_Type := 16#0A04#;

   Blob_Reset : constant Operations.Operation_Type := 16#0400#;

   Blob_Create : constant Operations.Operation_Type := 16#0401#;

   Blob_Read : constant Operations.Operation_Type := 16#0402#;

   Blob_Write : constant Operations.Operation_Type := 16#0403#;

end Tkmrpc.Operations.Ike;
