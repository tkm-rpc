package Tkmrpc
is

   --  Size of request/response messages in bytes.
   Message_Size : constant := 2048;

end Tkmrpc;
