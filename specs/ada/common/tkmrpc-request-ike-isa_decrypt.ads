with Tkmrpc.Types;
with Tkmrpc.Operations.Ike;

package Tkmrpc.Request.Ike.Isa_Decrypt is

   Data_Size : constant := 2030;

   type Data_Type is record
      Isa_Id               : Types.Isa_Id_Type;
      Aad_Len              : Types.Aad_Len_Type;
      Aad_Iv_Encrypted_Icv : Types.Aad_Iv_Encrypted_Icv_Type;
   end record;

   for Data_Type use record
      Isa_Id               at  0 range 0 ..    (8 * 8) - 1;
      Aad_Len              at  8 range 0 ..    (2 * 8) - 1;
      Aad_Iv_Encrypted_Icv at 10 range 0 .. (2020 * 8) - 1;
   end record;
   for Data_Type'Size use Data_Size * 8;

   Padding_Size : constant := Request.Body_Size - Data_Size;
   subtype Padding_Type is Types.Byte_Sequence (1 .. Padding_Size);

   type Request_Type is record
      Header  : Request.Header_Type;
      Data    : Data_Type;
      Padding : Padding_Type;
   end record;

   for Request_Type use record
      Header at                   0 range 0 .. (Request.Header_Size * 8) - 1;
      Data   at Request.Header_Size range 0 ..           (Data_Size * 8) - 1;
      Padding at Request.Header_Size + Data_Size range 0 ..
          (Padding_Size * 8) - 1;
   end record;
   for Request_Type'Size use Request.Request_Size * 8;

   Null_Request : constant Request_Type :=
     Request_Type'
       (Header =>
          Request.Header_Type'
            (Operation => Operations.Ike.Isa_Decrypt, Request_Id => 0),
        Data =>
          Data_Type'
            (Isa_Id               => Types.Isa_Id_Type'First,
             Aad_Len              => Types.Aad_Len_Type'First,
             Aad_Iv_Encrypted_Icv => Types.Null_Aad_Iv_Encrypted_Icv_Type),
        Padding => Padding_Type'(others => 0));

end Tkmrpc.Request.Ike.Isa_Decrypt;
