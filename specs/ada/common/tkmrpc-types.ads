with Interfaces;

package Tkmrpc.Types is

   subtype Byte is Interfaces.Unsigned_8;
   subtype Unsigned_8 is Interfaces.Unsigned_8;
   subtype Unsigned_16 is Interfaces.Unsigned_16;
   subtype Unsigned_32 is Interfaces.Unsigned_32;
   subtype Unsigned_64 is Interfaces.Unsigned_64;
   subtype Sequence_Range is Natural range 0 .. 2**31 - 2;
   for Sequence_Range'Object_Size use 4 * 8;
   subtype Byte_Sequence_Range is Sequence_Range;
   type Byte_Sequence is array (Sequence_Range range <>) of Unsigned_8;
   subtype U8_Sequence is Byte_Sequence;
   type U16_Sequence is array (Sequence_Range range <>) of Unsigned_16;
   type U32_Sequence is array (Sequence_Range range <>) of Unsigned_32;
   type U64_Sequence is array (Sequence_Range range <>) of Unsigned_64;

   type Request_Id_Type is new Interfaces.Unsigned_64;
   Null_Request_Id_Type : constant Request_Id_Type := Request_Id_Type'First;

   type Version_Type is new Interfaces.Unsigned_64;
   Null_Version_Type : constant Version_Type := Version_Type'First;

   type Active_Requests_Type is new Interfaces.Unsigned_64;
   Null_Active_Requests_Type : constant Active_Requests_Type :=
     Active_Requests_Type'First;

   type Authag_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Authag_Id_Type : constant Authag_Id_Type := Authag_Id_Type'First;

   type Cag_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Cag_Id_Type : constant Cag_Id_Type := Cag_Id_Type'First;

   type Li_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Li_Id_Type : constant Li_Id_Type := Li_Id_Type'First;

   type Ri_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Ri_Id_Type : constant Ri_Id_Type := Ri_Id_Type'First;

   type Iag_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Iag_Id_Type : constant Iag_Id_Type := Iag_Id_Type'First;

   type Eag_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Eag_Id_Type : constant Eag_Id_Type := Eag_Id_Type'First;

   type Keag_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Keag_Id_Type : constant Keag_Id_Type := Keag_Id_Type'First;

   type Sp_Id_Type is new Interfaces.Unsigned_32 range 1 .. 100;
   Null_Sp_Id_Type : constant Sp_Id_Type := Sp_Id_Type'First;

   type Authp_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Authp_Id_Type : constant Authp_Id_Type := Authp_Id_Type'First;

   type Kep_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Kep_Id_Type : constant Kep_Id_Type := Kep_Id_Type'First;

   type Autha_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Autha_Id_Type : constant Autha_Id_Type := Autha_Id_Type'First;

   type Ca_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Ca_Id_Type : constant Ca_Id_Type := Ca_Id_Type'First;

   type Lc_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Lc_Id_Type : constant Lc_Id_Type := Lc_Id_Type'First;

   type Ia_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Ia_Id_Type : constant Ia_Id_Type := Ia_Id_Type'First;

   type Ea_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Ea_Id_Type : constant Ea_Id_Type := Ea_Id_Type'First;

   type Kea_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Kea_Id_Type : constant Kea_Id_Type := Kea_Id_Type'First;

   type Nc_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Nc_Id_Type : constant Nc_Id_Type := Nc_Id_Type'First;

   type Ke_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Ke_Id_Type : constant Ke_Id_Type := Ke_Id_Type'First;

   type Cc_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Cc_Id_Type : constant Cc_Id_Type := Cc_Id_Type'First;

   type Ae_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Ae_Id_Type : constant Ae_Id_Type := Ae_Id_Type'First;

   type Isa_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Isa_Id_Type : constant Isa_Id_Type := Isa_Id_Type'First;

   type Esa_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Esa_Id_Type : constant Esa_Id_Type := Esa_Id_Type'First;

   type Esp_Enc_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Esp_Enc_Id_Type : constant Esp_Enc_Id_Type := Esp_Enc_Id_Type'First;

   type Esp_Dec_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Esp_Dec_Id_Type : constant Esp_Dec_Id_Type := Esp_Dec_Id_Type'First;

   type Esp_Map_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Esp_Map_Id_Type : constant Esp_Map_Id_Type := Esp_Map_Id_Type'First;

   type Abs_Time_Type is new Interfaces.Unsigned_64;
   Null_Abs_Time_Type : constant Abs_Time_Type := Abs_Time_Type'First;

   type Rel_Time_Type is new Interfaces.Unsigned_64;
   Null_Rel_Time_Type : constant Rel_Time_Type := Rel_Time_Type'First;

   type Duration_Type is new Interfaces.Unsigned_64;
   Null_Duration_Type : constant Duration_Type := Duration_Type'First;

   type Counter_Type is new Interfaces.Unsigned_64;
   Null_Counter_Type : constant Counter_Type := Counter_Type'First;

   type Pfs_Flag_Type is new Interfaces.Unsigned_64;
   Null_Pfs_Flag_Type : constant Pfs_Flag_Type := Pfs_Flag_Type'First;

   type Cc_Time_Flag_Type is new Interfaces.Unsigned_64;
   Null_Cc_Time_Flag_Type : constant Cc_Time_Flag_Type :=
     Cc_Time_Flag_Type'First;

   type Expiry_Flag_Type is new Interfaces.Unsigned_8;
   Null_Expiry_Flag_Type : constant Expiry_Flag_Type :=
     Expiry_Flag_Type'First;

   type Auth_Algorithm_Type is new Interfaces.Unsigned_64;
   Null_Auth_Algorithm_Type : constant Auth_Algorithm_Type :=
     Auth_Algorithm_Type'First;

   type Ke_Algorithm_Type is new Interfaces.Unsigned_16;
   Null_Ke_Algorithm_Type : constant Ke_Algorithm_Type :=
     Ke_Algorithm_Type'First;

   type Prf_Algorithm_Type is new Interfaces.Unsigned_16;
   Null_Prf_Algorithm_Type : constant Prf_Algorithm_Type :=
     Prf_Algorithm_Type'First;

   type Int_Algorithm_Type is new Interfaces.Unsigned_16;
   Null_Int_Algorithm_Type : constant Int_Algorithm_Type :=
     Int_Algorithm_Type'First;

   type Enc_Algorithm_Type is new Interfaces.Unsigned_16;
   Null_Enc_Algorithm_Type : constant Enc_Algorithm_Type :=
     Enc_Algorithm_Type'First;

   type Key_Length_Bits_Type is new Interfaces.Unsigned_64;
   Null_Key_Length_Bits_Type : constant Key_Length_Bits_Type :=
     Key_Length_Bits_Type'First;

   type Block_Length_Bits_Type is new Interfaces.Unsigned_64;
   Null_Block_Length_Bits_Type : constant Block_Length_Bits_Type :=
     Block_Length_Bits_Type'First;

   type Protocol_Type is new Interfaces.Unsigned_32;
   Null_Protocol_Type : constant Protocol_Type := Protocol_Type'First;

   type Init_Type is new Interfaces.Unsigned_64;
   Null_Init_Type : constant Init_Type := Init_Type'First;

   type Ike_Spi_Type is new Interfaces.Unsigned_64;
   Null_Ike_Spi_Type : constant Ike_Spi_Type := Ike_Spi_Type'First;

   type Esp_Spi_Type is new Interfaces.Unsigned_32;
   Null_Esp_Spi_Type : constant Esp_Spi_Type := Esp_Spi_Type'First;

   type Esa_Flags_Type is new Interfaces.Unsigned_64;
   Null_Esa_Flags_Type : constant Esa_Flags_Type := Esa_Flags_Type'First;

   type Nonce_Length_Type is new Interfaces.Unsigned_64;
   Null_Nonce_Length_Type : constant Nonce_Length_Type :=
     Nonce_Length_Type'First;

   subtype Init_Message_Type_Range is Sequence_Range range 1 .. 1500;
   subtype Init_Message_Type_Data_Type is
     U8_Sequence (Init_Message_Type_Range);

   type Init_Message_Type is record
      Size : Init_Message_Type_Range;
      Data : Init_Message_Type_Data_Type;
   end record;

   for Init_Message_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (1500 * 8) - 1;
   end record;
   for Init_Message_Type'Size use 1504 * 8;

   Null_Init_Message_Type : constant Init_Message_Type :=
     Init_Message_Type'
       (Size => Init_Message_Type_Range'First,
        Data => Init_Message_Type_Data_Type'(others => 0));

   subtype Certificate_Type_Range is Sequence_Range range 1 .. 2000;
   subtype Certificate_Type_Data_Type is
     U8_Sequence (Certificate_Type_Range);

   type Certificate_Type is record
      Size : Certificate_Type_Range;
      Data : Certificate_Type_Data_Type;
   end record;

   for Certificate_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (2000 * 8) - 1;
   end record;
   for Certificate_Type'Size use 2004 * 8;

   Null_Certificate_Type : constant Certificate_Type :=
     Certificate_Type'
       (Size => Certificate_Type_Range'First,
        Data => Certificate_Type_Data_Type'(others => 0));

   subtype Nonce_Type_Range is Sequence_Range range 1 .. 256;
   subtype Nonce_Type_Data_Type is U8_Sequence (Nonce_Type_Range);

   type Nonce_Type is record
      Size : Nonce_Type_Range;
      Data : Nonce_Type_Data_Type;
   end record;

   for Nonce_Type use record
      Size at 0 range 0 ..            31;
      Data at 4 range 0 .. (256 * 8) - 1;
   end record;
   for Nonce_Type'Size use 260 * 8;

   Null_Nonce_Type : constant Nonce_Type :=
     Nonce_Type'
       (Size => Nonce_Type_Range'First,
        Data => Nonce_Type_Data_Type'(others => 0));

   subtype Ke_Pubvalue_Type_Range is Sequence_Range range 1 .. 512;
   subtype Ke_Pubvalue_Type_Data_Type is
     U8_Sequence (Ke_Pubvalue_Type_Range);

   type Ke_Pubvalue_Type is record
      Size : Ke_Pubvalue_Type_Range;
      Data : Ke_Pubvalue_Type_Data_Type;
   end record;

   for Ke_Pubvalue_Type use record
      Size at 0 range 0 ..            31;
      Data at 4 range 0 .. (512 * 8) - 1;
   end record;
   for Ke_Pubvalue_Type'Size use 516 * 8;

   Null_Ke_Pubvalue_Type : constant Ke_Pubvalue_Type :=
     Ke_Pubvalue_Type'
       (Size => Ke_Pubvalue_Type_Range'First,
        Data => Ke_Pubvalue_Type_Data_Type'(others => 0));

   subtype Ke_Priv_Type_Range is Sequence_Range range 1 .. 512;
   subtype Ke_Priv_Type_Data_Type is U8_Sequence (Ke_Priv_Type_Range);

   type Ke_Priv_Type is record
      Size : Ke_Priv_Type_Range;
      Data : Ke_Priv_Type_Data_Type;
   end record;

   for Ke_Priv_Type use record
      Size at 0 range 0 ..            31;
      Data at 4 range 0 .. (512 * 8) - 1;
   end record;
   for Ke_Priv_Type'Size use 516 * 8;

   Null_Ke_Priv_Type : constant Ke_Priv_Type :=
     Ke_Priv_Type'
       (Size => Ke_Priv_Type_Range'First,
        Data => Ke_Priv_Type_Data_Type'(others => 0));

   subtype Ke_Key_Type_Range is Sequence_Range range 1 .. 512;
   subtype Ke_Key_Type_Data_Type is U8_Sequence (Ke_Key_Type_Range);

   type Ke_Key_Type is record
      Size : Ke_Key_Type_Range;
      Data : Ke_Key_Type_Data_Type;
   end record;

   for Ke_Key_Type use record
      Size at 0 range 0 ..            31;
      Data at 4 range 0 .. (512 * 8) - 1;
   end record;
   for Ke_Key_Type'Size use 516 * 8;

   Null_Ke_Key_Type : constant Ke_Key_Type :=
     Ke_Key_Type'
       (Size => Ke_Key_Type_Range'First,
        Data => Ke_Key_Type_Data_Type'(others => 0));

   subtype Key_Type_Range is Sequence_Range range 1 .. 64;
   subtype Key_Type_Data_Type is U8_Sequence (Key_Type_Range);

   type Key_Type is record
      Size : Key_Type_Range;
      Data : Key_Type_Data_Type;
   end record;

   for Key_Type use record
      Size at 0 range 0 ..           31;
      Data at 4 range 0 .. (64 * 8) - 1;
   end record;
   for Key_Type'Size use 68 * 8;

   Null_Key_Type : constant Key_Type :=
     Key_Type'
       (Size => Key_Type_Range'First,
        Data => Key_Type_Data_Type'(others => 0));

   subtype Intauth_Type_Range is Sequence_Range range 1 .. 64;
   subtype Intauth_Type_Data_Type is U8_Sequence (Intauth_Type_Range);

   type Intauth_Type is record
      Size : Intauth_Type_Range;
      Data : Intauth_Type_Data_Type;
   end record;

   for Intauth_Type use record
      Size at 0 range 0 ..           31;
      Data at 4 range 0 .. (64 * 8) - 1;
   end record;
   for Intauth_Type'Size use 68 * 8;

   Null_Intauth_Type : constant Intauth_Type :=
     Intauth_Type'
       (Size => Intauth_Type_Range'First,
        Data => Intauth_Type_Data_Type'(others => 0));

   type Intauth_Count_Type is new Interfaces.Unsigned_32;
   Null_Intauth_Count_Type : constant Intauth_Count_Type :=
     Intauth_Count_Type'First;

   subtype Identity_Type_Range is Sequence_Range range 1 .. 64;
   subtype Identity_Type_Data_Type is U8_Sequence (Identity_Type_Range);

   type Identity_Type is record
      Size : Identity_Type_Range;
      Data : Identity_Type_Data_Type;
   end record;

   for Identity_Type use record
      Size at 0 range 0 ..           31;
      Data at 4 range 0 .. (64 * 8) - 1;
   end record;
   for Identity_Type'Size use 68 * 8;

   Null_Identity_Type : constant Identity_Type :=
     Identity_Type'
       (Size => Identity_Type_Range'First,
        Data => Identity_Type_Data_Type'(others => 0));

   subtype Signature_Type_Range is Sequence_Range range 1 .. 384;
   subtype Signature_Type_Data_Type is U8_Sequence (Signature_Type_Range);

   type Signature_Type is record
      Size : Signature_Type_Range;
      Data : Signature_Type_Data_Type;
   end record;

   for Signature_Type use record
      Size at 0 range 0 ..            31;
      Data at 4 range 0 .. (384 * 8) - 1;
   end record;
   for Signature_Type'Size use 388 * 8;

   Null_Signature_Type : constant Signature_Type :=
     Signature_Type'
       (Size => Signature_Type_Range'First,
        Data => Signature_Type_Data_Type'(others => 0));

   subtype Auth_Parameter_Type_Range is Sequence_Range range 1 .. 1024;
   subtype Auth_Parameter_Type_Data_Type is
     U8_Sequence (Auth_Parameter_Type_Range);

   type Auth_Parameter_Type is record
      Size : Auth_Parameter_Type_Range;
      Data : Auth_Parameter_Type_Data_Type;
   end record;

   for Auth_Parameter_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (1024 * 8) - 1;
   end record;
   for Auth_Parameter_Type'Size use 1028 * 8;

   Null_Auth_Parameter_Type : constant Auth_Parameter_Type :=
     Auth_Parameter_Type'
       (Size => Auth_Parameter_Type_Range'First,
        Data => Auth_Parameter_Type_Data_Type'(others => 0));

   subtype Ke_Parameter_Type_Range is Sequence_Range range 1 .. 1024;
   subtype Ke_Parameter_Type_Data_Type is
     U8_Sequence (Ke_Parameter_Type_Range);

   type Ke_Parameter_Type is record
      Size : Ke_Parameter_Type_Range;
      Data : Ke_Parameter_Type_Data_Type;
   end record;

   for Ke_Parameter_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (1024 * 8) - 1;
   end record;
   for Ke_Parameter_Type'Size use 1028 * 8;

   Null_Ke_Parameter_Type : constant Ke_Parameter_Type :=
     Ke_Parameter_Type'
       (Size => Ke_Parameter_Type_Range'First,
        Data => Ke_Parameter_Type_Data_Type'(others => 0));

   type Aad_Len_Type is new Interfaces.Unsigned_16;
   Null_Aad_Len_Type : constant Aad_Len_Type := Aad_Len_Type'First;

   type Iv_Len_Type is new Interfaces.Unsigned_16;
   Null_Iv_Len_Type : constant Iv_Len_Type := Iv_Len_Type'First;

   type Icv_Len_Type is new Interfaces.Unsigned_16;
   Null_Icv_Len_Type : constant Icv_Len_Type := Icv_Len_Type'First;

   type Block_Len_Type is new Interfaces.Unsigned_16;
   Null_Block_Len_Type : constant Block_Len_Type := Block_Len_Type'First;

   subtype Aad_Plain_Type_Range is Sequence_Range range 1 .. 2016;
   subtype Aad_Plain_Type_Data_Type is U8_Sequence (Aad_Plain_Type_Range);

   type Aad_Plain_Type is record
      Size : Aad_Plain_Type_Range;
      Data : Aad_Plain_Type_Data_Type;
   end record;

   for Aad_Plain_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (2016 * 8) - 1;
   end record;
   for Aad_Plain_Type'Size use 2020 * 8;

   Null_Aad_Plain_Type : constant Aad_Plain_Type :=
     Aad_Plain_Type'
       (Size => Aad_Plain_Type_Range'First,
        Data => Aad_Plain_Type_Data_Type'(others => 0));

   subtype Iv_Encrypted_Icv_Type_Range is Sequence_Range range 1 .. 2019;
   subtype Iv_Encrypted_Icv_Type_Data_Type is
     U8_Sequence (Iv_Encrypted_Icv_Type_Range);

   type Iv_Encrypted_Icv_Type is record
      Size : Iv_Encrypted_Icv_Type_Range;
      Data : Iv_Encrypted_Icv_Type_Data_Type;
   end record;

   for Iv_Encrypted_Icv_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (2019 * 8) - 1;
   end record;
   for Iv_Encrypted_Icv_Type'Size use 2023 * 8;

   Null_Iv_Encrypted_Icv_Type : constant Iv_Encrypted_Icv_Type :=
     Iv_Encrypted_Icv_Type'
       (Size => Iv_Encrypted_Icv_Type_Range'First,
        Data => Iv_Encrypted_Icv_Type_Data_Type'(others => 0));

   subtype Aad_Iv_Encrypted_Icv_Type_Range is Sequence_Range range 1 .. 2016;
   subtype Aad_Iv_Encrypted_Icv_Type_Data_Type is
     U8_Sequence (Aad_Iv_Encrypted_Icv_Type_Range);

   type Aad_Iv_Encrypted_Icv_Type is record
      Size : Aad_Iv_Encrypted_Icv_Type_Range;
      Data : Aad_Iv_Encrypted_Icv_Type_Data_Type;
   end record;

   for Aad_Iv_Encrypted_Icv_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (2016 * 8) - 1;
   end record;
   for Aad_Iv_Encrypted_Icv_Type'Size use 2020 * 8;

   Null_Aad_Iv_Encrypted_Icv_Type : constant Aad_Iv_Encrypted_Icv_Type :=
     Aad_Iv_Encrypted_Icv_Type'
       (Size => Aad_Iv_Encrypted_Icv_Type_Range'First,
        Data => Aad_Iv_Encrypted_Icv_Type_Data_Type'(others => 0));

   subtype Decrypted_Type_Range is Sequence_Range range 1 .. 2016;
   subtype Decrypted_Type_Data_Type is U8_Sequence (Decrypted_Type_Range);

   type Decrypted_Type is record
      Size : Decrypted_Type_Range;
      Data : Decrypted_Type_Data_Type;
   end record;

   for Decrypted_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (2016 * 8) - 1;
   end record;
   for Decrypted_Type'Size use 2020 * 8;

   Null_Decrypted_Type : constant Decrypted_Type :=
     Decrypted_Type'
       (Size => Decrypted_Type_Range'First,
        Data => Decrypted_Type_Data_Type'(others => 0));

   type Blob_Id_Type is new Interfaces.Unsigned_64 range 1 .. 100;
   Null_Blob_Id_Type : constant Blob_Id_Type := Blob_Id_Type'First;

   type Blob_Length_Type is new Interfaces.Unsigned_32;
   Null_Blob_Length_Type : constant Blob_Length_Type :=
     Blob_Length_Type'First;

   type Blob_Offset_Type is new Interfaces.Unsigned_32;
   Null_Blob_Offset_Type : constant Blob_Offset_Type :=
     Blob_Offset_Type'First;

   type Blob_Out_Bytes_Type is new U8_Sequence (1 .. 2024);
   for Blob_Out_Bytes_Type'Size use 2024 * 8;

   Null_Blob_Out_Bytes_Type : constant Blob_Out_Bytes_Type := (others => 0);

   subtype Blob_In_Bytes_Type_Range is Sequence_Range range 1 .. 2016;
   subtype Blob_In_Bytes_Type_Data_Type is
     U8_Sequence (Blob_In_Bytes_Type_Range);

   type Blob_In_Bytes_Type is record
      Size : Blob_In_Bytes_Type_Range;
      Data : Blob_In_Bytes_Type_Data_Type;
   end record;

   for Blob_In_Bytes_Type use record
      Size at 0 range 0 ..             31;
      Data at 4 range 0 .. (2016 * 8) - 1;
   end record;
   for Blob_In_Bytes_Type'Size use 2020 * 8;

   Null_Blob_In_Bytes_Type : constant Blob_In_Bytes_Type :=
     Blob_In_Bytes_Type'
       (Size => Blob_In_Bytes_Type_Range'First,
        Data => Blob_In_Bytes_Type_Data_Type'(others => 0));

   type Inbound_Flag_Type is new Interfaces.Unsigned_8;
   Null_Inbound_Flag_Type : constant Inbound_Flag_Type :=
     Inbound_Flag_Type'First;

   subtype Ke_Ids_Type_Range is Sequence_Range range 1 .. 8;
   subtype Ke_Ids_Type_Data_Type is U64_Sequence (Ke_Ids_Type_Range);

   type Ke_Ids_Type is record
      Size : Ke_Ids_Type_Range;
      Data : Ke_Ids_Type_Data_Type;
   end record;

   for Ke_Ids_Type use record
      Size at 0 range 0 ..           31;
      Data at 4 range 0 .. (8 * 64) - 1;
   end record;
   for Ke_Ids_Type'Size use 68 * 8;

   Null_Ke_Ids_Type : constant Ke_Ids_Type :=
     Ke_Ids_Type'
       (Size => Ke_Ids_Type_Range'First,
        Data => Ke_Ids_Type_Data_Type'(others => 0));

end Tkmrpc.Types;
