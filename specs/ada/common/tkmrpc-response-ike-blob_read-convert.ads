with Ada.Unchecked_Conversion;

package Tkmrpc.Response.Ike.Blob_Read.Convert is

   function To_Response is new Ada.Unchecked_Conversion
     (Source => Blob_Read.Response_Type, Target => Response.Data_Type);

   function From_Response is new Ada.Unchecked_Conversion
     (Source => Response.Data_Type, Target => Blob_Read.Response_Type);

end Tkmrpc.Response.Ike.Blob_Read.Convert;
