pragma Style_Checks ("-o");

with Tkmrpc.Transport.Client;
with Tkmrpc.Request.Ike.Tkm_Version.Convert;
with Tkmrpc.Request.Ike.Tkm_Limits.Convert;
with Tkmrpc.Request.Ike.Tkm_Reset.Convert;
with Tkmrpc.Request.Ike.Nc_Reset.Convert;
with Tkmrpc.Request.Ike.Nc_Create.Convert;
with Tkmrpc.Request.Ike.Ke_Reset.Convert;
with Tkmrpc.Request.Ike.Ke_Get.Convert;
with Tkmrpc.Request.Ike.Ke_Set.Convert;
with Tkmrpc.Request.Ike.Cc_Reset.Convert;
with Tkmrpc.Request.Ike.Cc_Check_Chain.Convert;
with Tkmrpc.Request.Ike.Cc_Add_Certificate.Convert;
with Tkmrpc.Request.Ike.Cc_Check_Ca.Convert;
with Tkmrpc.Request.Ike.Ae_Reset.Convert;
with Tkmrpc.Request.Ike.Isa_Reset.Convert;
with Tkmrpc.Request.Ike.Isa_Create.Convert;
with Tkmrpc.Request.Ike.Isa_Sign.Convert;
with Tkmrpc.Request.Ike.Isa_Auth.Convert;
with Tkmrpc.Request.Ike.Isa_Create_Child.Convert;
with Tkmrpc.Request.Ike.Isa_Skip_Create_First.Convert;
with Tkmrpc.Request.Ike.Isa_Encrypt.Convert;
with Tkmrpc.Request.Ike.Isa_Decrypt.Convert;
with Tkmrpc.Request.Ike.Isa_Update.Convert;
with Tkmrpc.Request.Ike.Isa_Int_Auth.Convert;
with Tkmrpc.Request.Ike.Esa_Reset.Convert;
with Tkmrpc.Request.Ike.Esa_Create.Convert;
with Tkmrpc.Request.Ike.Esa_Create_No_Pfs.Convert;
with Tkmrpc.Request.Ike.Esa_Create_First.Convert;
with Tkmrpc.Request.Ike.Esa_Select.Convert;
with Tkmrpc.Request.Ike.Blob_Reset.Convert;
with Tkmrpc.Request.Ike.Blob_Create.Convert;
with Tkmrpc.Request.Ike.Blob_Read.Convert;
with Tkmrpc.Request.Ike.Blob_Write.Convert;
with Tkmrpc.Response.Ike.Tkm_Version.Convert;
with Tkmrpc.Response.Ike.Tkm_Limits.Convert;
with Tkmrpc.Response.Ike.Tkm_Reset.Convert;
with Tkmrpc.Response.Ike.Nc_Reset.Convert;
with Tkmrpc.Response.Ike.Nc_Create.Convert;
with Tkmrpc.Response.Ike.Ke_Reset.Convert;
with Tkmrpc.Response.Ike.Ke_Get.Convert;
with Tkmrpc.Response.Ike.Ke_Set.Convert;
with Tkmrpc.Response.Ike.Cc_Reset.Convert;
with Tkmrpc.Response.Ike.Cc_Check_Chain.Convert;
with Tkmrpc.Response.Ike.Cc_Add_Certificate.Convert;
with Tkmrpc.Response.Ike.Cc_Check_Ca.Convert;
with Tkmrpc.Response.Ike.Ae_Reset.Convert;
with Tkmrpc.Response.Ike.Isa_Reset.Convert;
with Tkmrpc.Response.Ike.Isa_Create.Convert;
with Tkmrpc.Response.Ike.Isa_Sign.Convert;
with Tkmrpc.Response.Ike.Isa_Auth.Convert;
with Tkmrpc.Response.Ike.Isa_Create_Child.Convert;
with Tkmrpc.Response.Ike.Isa_Skip_Create_First.Convert;
with Tkmrpc.Response.Ike.Isa_Encrypt.Convert;
with Tkmrpc.Response.Ike.Isa_Decrypt.Convert;
with Tkmrpc.Response.Ike.Isa_Update.Convert;
with Tkmrpc.Response.Ike.Isa_Int_Auth.Convert;
with Tkmrpc.Response.Ike.Esa_Reset.Convert;
with Tkmrpc.Response.Ike.Esa_Create.Convert;
with Tkmrpc.Response.Ike.Esa_Create_No_Pfs.Convert;
with Tkmrpc.Response.Ike.Esa_Create_First.Convert;
with Tkmrpc.Response.Ike.Esa_Select.Convert;
with Tkmrpc.Response.Ike.Blob_Reset.Convert;
with Tkmrpc.Response.Ike.Blob_Create.Convert;
with Tkmrpc.Response.Ike.Blob_Read.Convert;
with Tkmrpc.Response.Ike.Blob_Write.Convert;

package body Tkmrpc.Clients.Ike is

   -------------------------------------------------------------------------

   procedure Init
     (Result  : out Results.Result_Type;
      Address :     Interfaces.C.Strings.Chars_Ptr) is separate;

   -------------------------------------------------------------------------

   procedure Finalize is separate;

   -------------------------------------------------------------------------

   procedure Ae_Reset
     (Result : out Results.Result_Type;
      Ae_Id  :     Types.Ae_Id_Type)
   is
      Req  : Request.Ike.Ae_Reset.Request_Type;
      Res  : Response.Ike.Ae_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Ae_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req            := Request.Ike.Ae_Reset.Null_Request;
      Req.Data.Ae_Id := Ae_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Ae_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Ae_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Ae_Reset;

   -------------------------------------------------------------------------

   procedure Blob_Create
     (Result  : out Results.Result_Type;
      Blob_Id :     Types.Blob_Id_Type;
      Length  :     Types.Blob_Length_Type)
   is
      Req  : Request.Ike.Blob_Create.Request_Type;
      Res  : Response.Ike.Blob_Create.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Blob_Id'Valid and Length'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req              := Request.Ike.Blob_Create.Null_Request;
      Req.Data.Blob_Id := Blob_Id;
      Req.Data.Length  := Length;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Blob_Create.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Blob_Create.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Blob_Create;

   -------------------------------------------------------------------------

   procedure Blob_Read
     (Result    : out Results.Result_Type;
      Blob_Id   :     Types.Blob_Id_Type;
      Offset    :     Types.Blob_Offset_Type;
      Length    :     Types.Blob_Length_Type;
      Blob_Data : out Types.Blob_Out_Bytes_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Blob_Read.Request_Type;
      Res  : Response.Ike.Blob_Read.Response_Type;
      Data : Response.Data_Type;
   begin
      Blob_Data := Types.Null_Blob_Out_Bytes_Type;
      if not (Blob_Id'Valid and Offset'Valid and Length'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req              := Request.Ike.Blob_Read.Null_Request;
      Req.Data.Blob_Id := Blob_Id;
      Req.Data.Offset  := Offset;
      Req.Data.Length  := Length;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Blob_Read.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Blob_Read.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Blob_Data := Res.Data.Blob_Data;
      end if;
   end Blob_Read;

   -------------------------------------------------------------------------

   procedure Blob_Reset
     (Result  : out Results.Result_Type;
      Blob_Id :     Types.Blob_Id_Type)
   is
      Req  : Request.Ike.Blob_Reset.Request_Type;
      Res  : Response.Ike.Blob_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Blob_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req              := Request.Ike.Blob_Reset.Null_Request;
      Req.Data.Blob_Id := Blob_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Blob_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Blob_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Blob_Reset;

   -------------------------------------------------------------------------

   procedure Blob_Write
     (Result    : out Results.Result_Type;
      Blob_Id   :     Types.Blob_Id_Type;
      Offset    :     Types.Blob_Offset_Type;
      Blob_Data :     Types.Blob_In_Bytes_Type)
   is
      Req  : Request.Ike.Blob_Write.Request_Type;
      Res  : Response.Ike.Blob_Write.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Blob_Id'Valid and Offset'Valid and Blob_Data.Size'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                := Request.Ike.Blob_Write.Null_Request;
      Req.Data.Blob_Id   := Blob_Id;
      Req.Data.Offset    := Offset;
      Req.Data.Blob_Data := Blob_Data;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Blob_Write.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Blob_Write.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Blob_Write;

   -------------------------------------------------------------------------

   procedure Cc_Add_Certificate
     (Result      : out Results.Result_Type;
      Cc_Id       :     Types.Cc_Id_Type;
      Autha_Id    :     Types.Autha_Id_Type;
      Certificate :     Types.Certificate_Type)
   is
      Req  : Request.Ike.Cc_Add_Certificate.Request_Type;
      Res  : Response.Ike.Cc_Add_Certificate.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Cc_Id'Valid and Autha_Id'Valid and Certificate.Size'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                  := Request.Ike.Cc_Add_Certificate.Null_Request;
      Req.Data.Cc_Id       := Cc_Id;
      Req.Data.Autha_Id    := Autha_Id;
      Req.Data.Certificate := Certificate;

      Transport.Client.Send_Receive
        (Req_Data =>
           Request.Ike.Cc_Add_Certificate.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res :=
        Response.Ike.Cc_Add_Certificate.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Cc_Add_Certificate;

   -------------------------------------------------------------------------

   procedure Cc_Check_Ca
     (Result         : out Results.Result_Type;
      Cc_Id          :     Types.Cc_Id_Type;
      Ca_Id          :     Types.Ca_Id_Type;
      Ca_Certificate :     Types.Certificate_Type)
   is
      Req  : Request.Ike.Cc_Check_Ca.Request_Type;
      Res  : Response.Ike.Cc_Check_Ca.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Cc_Id'Valid and Ca_Id'Valid and Ca_Certificate.Size'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                     := Request.Ike.Cc_Check_Ca.Null_Request;
      Req.Data.Cc_Id          := Cc_Id;
      Req.Data.Ca_Id          := Ca_Id;
      Req.Data.Ca_Certificate := Ca_Certificate;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Cc_Check_Ca.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Cc_Check_Ca.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Cc_Check_Ca;

   -------------------------------------------------------------------------

   procedure Cc_Check_Chain
     (Result : out Results.Result_Type;
      Cc_Id  :     Types.Cc_Id_Type;
      Ri_Id  :     Types.Ri_Id_Type)
   is
      Req  : Request.Ike.Cc_Check_Chain.Request_Type;
      Res  : Response.Ike.Cc_Check_Chain.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Cc_Id'Valid and Ri_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req            := Request.Ike.Cc_Check_Chain.Null_Request;
      Req.Data.Cc_Id := Cc_Id;
      Req.Data.Ri_Id := Ri_Id;

      Transport.Client.Send_Receive
        (Req_Data =>
           Request.Ike.Cc_Check_Chain.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Cc_Check_Chain.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Cc_Check_Chain;

   -------------------------------------------------------------------------

   procedure Cc_Reset
     (Result : out Results.Result_Type;
      Cc_Id  :     Types.Cc_Id_Type)
   is
      Req  : Request.Ike.Cc_Reset.Request_Type;
      Res  : Response.Ike.Cc_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Cc_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req            := Request.Ike.Cc_Reset.Null_Request;
      Req.Data.Cc_Id := Cc_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Cc_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Cc_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Cc_Reset;

   -------------------------------------------------------------------------

   procedure Esa_Create
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Ke_Ids      :     Types.Ke_Ids_Type;
      Nc_Loc_Id   :     Types.Nc_Id_Type;
      Nonce_Rem   :     Types.Nonce_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type)
   is
      Req  : Request.Ike.Esa_Create.Request_Type;
      Res  : Response.Ike.Esa_Create.Response_Type;
      Data : Response.Data_Type;
   begin
      if not
        (Esa_Id'Valid and Isa_Id'Valid and Sp_Id'Valid and Ea_Id'Valid and
         Ke_Ids.Size'Valid and Nc_Loc_Id'Valid and Nonce_Rem.Size'Valid and
         Esa_Flags'Valid and Esp_Spi_Loc'Valid and Esp_Spi_Rem'Valid)
      then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                  := Request.Ike.Esa_Create.Null_Request;
      Req.Data.Esa_Id      := Esa_Id;
      Req.Data.Isa_Id      := Isa_Id;
      Req.Data.Sp_Id       := Sp_Id;
      Req.Data.Ea_Id       := Ea_Id;
      Req.Data.Ke_Ids      := Ke_Ids;
      Req.Data.Nc_Loc_Id   := Nc_Loc_Id;
      Req.Data.Nonce_Rem   := Nonce_Rem;
      Req.Data.Esa_Flags   := Esa_Flags;
      Req.Data.Esp_Spi_Loc := Esp_Spi_Loc;
      Req.Data.Esp_Spi_Rem := Esp_Spi_Rem;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Esa_Create.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Esa_Create.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Esa_Create;

   -------------------------------------------------------------------------

   procedure Esa_Create_First
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type)
   is
      Req  : Request.Ike.Esa_Create_First.Request_Type;
      Res  : Response.Ike.Esa_Create_First.Response_Type;
      Data : Response.Data_Type;
   begin
      if not
        (Esa_Id'Valid and Isa_Id'Valid and Sp_Id'Valid and Ea_Id'Valid and
         Esa_Flags'Valid and Esp_Spi_Loc'Valid and Esp_Spi_Rem'Valid)
      then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                  := Request.Ike.Esa_Create_First.Null_Request;
      Req.Data.Esa_Id      := Esa_Id;
      Req.Data.Isa_Id      := Isa_Id;
      Req.Data.Sp_Id       := Sp_Id;
      Req.Data.Ea_Id       := Ea_Id;
      Req.Data.Esa_Flags   := Esa_Flags;
      Req.Data.Esp_Spi_Loc := Esp_Spi_Loc;
      Req.Data.Esp_Spi_Rem := Esp_Spi_Rem;

      Transport.Client.Send_Receive
        (Req_Data =>
           Request.Ike.Esa_Create_First.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Esa_Create_First.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Esa_Create_First;

   -------------------------------------------------------------------------

   procedure Esa_Create_No_Pfs
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Nc_Loc_Id   :     Types.Nc_Id_Type;
      Nonce_Rem   :     Types.Nonce_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type)
   is
      Req  : Request.Ike.Esa_Create_No_Pfs.Request_Type;
      Res  : Response.Ike.Esa_Create_No_Pfs.Response_Type;
      Data : Response.Data_Type;
   begin
      if not
        (Esa_Id'Valid and Isa_Id'Valid and Sp_Id'Valid and Ea_Id'Valid and
         Nc_Loc_Id'Valid and Nonce_Rem.Size'Valid and Esa_Flags'Valid and
         Esp_Spi_Loc'Valid and Esp_Spi_Rem'Valid)
      then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                  := Request.Ike.Esa_Create_No_Pfs.Null_Request;
      Req.Data.Esa_Id      := Esa_Id;
      Req.Data.Isa_Id      := Isa_Id;
      Req.Data.Sp_Id       := Sp_Id;
      Req.Data.Ea_Id       := Ea_Id;
      Req.Data.Nc_Loc_Id   := Nc_Loc_Id;
      Req.Data.Nonce_Rem   := Nonce_Rem;
      Req.Data.Esa_Flags   := Esa_Flags;
      Req.Data.Esp_Spi_Loc := Esp_Spi_Loc;
      Req.Data.Esp_Spi_Rem := Esp_Spi_Rem;

      Transport.Client.Send_Receive
        (Req_Data =>
           Request.Ike.Esa_Create_No_Pfs.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res :=
        Response.Ike.Esa_Create_No_Pfs.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Esa_Create_No_Pfs;

   -------------------------------------------------------------------------

   procedure Esa_Reset
     (Result : out Results.Result_Type;
      Esa_Id :     Types.Esa_Id_Type)
   is
      Req  : Request.Ike.Esa_Reset.Request_Type;
      Res  : Response.Ike.Esa_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Esa_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req             := Request.Ike.Esa_Reset.Null_Request;
      Req.Data.Esa_Id := Esa_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Esa_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Esa_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Esa_Reset;

   -------------------------------------------------------------------------

   procedure Esa_Select
     (Result : out Results.Result_Type;
      Esa_Id :     Types.Esa_Id_Type)
   is
      Req  : Request.Ike.Esa_Select.Request_Type;
      Res  : Response.Ike.Esa_Select.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Esa_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req             := Request.Ike.Esa_Select.Null_Request;
      Req.Data.Esa_Id := Esa_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Esa_Select.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Esa_Select.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Esa_Select;

   -------------------------------------------------------------------------

   procedure Isa_Auth
     (Result       : out Results.Result_Type;
      Isa_Id       :     Types.Isa_Id_Type;
      Cc_Id        :     Types.Cc_Id_Type;
      Init_Message :     Types.Init_Message_Type;
      Signature    :     Types.Signature_Type)
   is
      Req  : Request.Ike.Isa_Auth.Request_Type;
      Res  : Response.Ike.Isa_Auth.Response_Type;
      Data : Response.Data_Type;
   begin
      if not
        (Isa_Id'Valid and Cc_Id'Valid and Init_Message.Size'Valid and
         Signature.Size'Valid)
      then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                   := Request.Ike.Isa_Auth.Null_Request;
      Req.Data.Isa_Id       := Isa_Id;
      Req.Data.Cc_Id        := Cc_Id;
      Req.Data.Init_Message := Init_Message;
      Req.Data.Signature    := Signature;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Isa_Auth.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Auth.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Isa_Auth;

   -------------------------------------------------------------------------

   procedure Isa_Create
     (Result    : out Results.Result_Type;
      Isa_Id    :     Types.Isa_Id_Type;
      Ae_Id     :     Types.Ae_Id_Type;
      Ia_Id     :     Types.Ia_Id_Type;
      Ke_Id     :     Types.Ke_Id_Type;
      Nc_Loc_Id :     Types.Nc_Id_Type;
      Nonce_Rem :     Types.Nonce_Type;
      Initiator :     Types.Init_Type;
      Spi_Loc   :     Types.Ike_Spi_Type;
      Spi_Rem   :     Types.Ike_Spi_Type;
      Block_Len : out Types.Block_Len_Type;
      Icv_Len   : out Types.Icv_Len_Type;
      Iv_Len    : out Types.Iv_Len_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Isa_Create.Request_Type;
      Res  : Response.Ike.Isa_Create.Response_Type;
      Data : Response.Data_Type;
   begin
      Block_Len := Types.Null_Block_Len_Type;
      Icv_Len   := Types.Null_Icv_Len_Type;
      Iv_Len    := Types.Null_Iv_Len_Type;
      if not
        (Isa_Id'Valid and Ae_Id'Valid and Ia_Id'Valid and Ke_Id'Valid and
         Nc_Loc_Id'Valid and Nonce_Rem.Size'Valid and Initiator'Valid and
         Spi_Loc'Valid and Spi_Rem'Valid)
      then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                := Request.Ike.Isa_Create.Null_Request;
      Req.Data.Isa_Id    := Isa_Id;
      Req.Data.Ae_Id     := Ae_Id;
      Req.Data.Ia_Id     := Ia_Id;
      Req.Data.Ke_Id     := Ke_Id;
      Req.Data.Nc_Loc_Id := Nc_Loc_Id;
      Req.Data.Nonce_Rem := Nonce_Rem;
      Req.Data.Initiator := Initiator;
      Req.Data.Spi_Loc   := Spi_Loc;
      Req.Data.Spi_Rem   := Spi_Rem;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Isa_Create.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Create.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Block_Len := Res.Data.Block_Len;
         Icv_Len   := Res.Data.Icv_Len;
         Iv_Len    := Res.Data.Iv_Len;
      end if;
   end Isa_Create;

   -------------------------------------------------------------------------

   procedure Isa_Create_Child
     (Result        : out Results.Result_Type;
      Isa_Id        :     Types.Isa_Id_Type;
      Parent_Isa_Id :     Types.Isa_Id_Type;
      Ia_Id         :     Types.Ia_Id_Type;
      Ke_Ids        :     Types.Ke_Ids_Type;
      Nc_Loc_Id     :     Types.Nc_Id_Type;
      Nonce_Rem     :     Types.Nonce_Type;
      Initiator     :     Types.Init_Type;
      Spi_Loc       :     Types.Ike_Spi_Type;
      Spi_Rem       :     Types.Ike_Spi_Type;
      Block_Len     : out Types.Block_Len_Type;
      Icv_Len       : out Types.Icv_Len_Type;
      Iv_Len        : out Types.Iv_Len_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Isa_Create_Child.Request_Type;
      Res  : Response.Ike.Isa_Create_Child.Response_Type;
      Data : Response.Data_Type;
   begin
      Block_Len := Types.Null_Block_Len_Type;
      Icv_Len   := Types.Null_Icv_Len_Type;
      Iv_Len    := Types.Null_Iv_Len_Type;
      if not
        (Isa_Id'Valid and Parent_Isa_Id'Valid and Ia_Id'Valid and
         Ke_Ids.Size'Valid and Nc_Loc_Id'Valid and Nonce_Rem.Size'Valid and
         Initiator'Valid and Spi_Loc'Valid and Spi_Rem'Valid)
      then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                    := Request.Ike.Isa_Create_Child.Null_Request;
      Req.Data.Isa_Id        := Isa_Id;
      Req.Data.Parent_Isa_Id := Parent_Isa_Id;
      Req.Data.Ia_Id         := Ia_Id;
      Req.Data.Ke_Ids        := Ke_Ids;
      Req.Data.Nc_Loc_Id     := Nc_Loc_Id;
      Req.Data.Nonce_Rem     := Nonce_Rem;
      Req.Data.Initiator     := Initiator;
      Req.Data.Spi_Loc       := Spi_Loc;
      Req.Data.Spi_Rem       := Spi_Rem;

      Transport.Client.Send_Receive
        (Req_Data =>
           Request.Ike.Isa_Create_Child.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Create_Child.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Block_Len := Res.Data.Block_Len;
         Icv_Len   := Res.Data.Icv_Len;
         Iv_Len    := Res.Data.Iv_Len;
      end if;
   end Isa_Create_Child;

   -------------------------------------------------------------------------

   procedure Isa_Decrypt
     (Result               : out Results.Result_Type;
      Isa_Id               :     Types.Isa_Id_Type;
      Aad_Len              :     Types.Aad_Len_Type;
      Aad_Iv_Encrypted_Icv :     Types.Aad_Iv_Encrypted_Icv_Type;
      Decrypted            : out Types.Decrypted_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Isa_Decrypt.Request_Type;
      Res  : Response.Ike.Isa_Decrypt.Response_Type;
      Data : Response.Data_Type;
   begin
      Decrypted := Types.Null_Decrypted_Type;
      if not
        (Isa_Id'Valid and Aad_Len'Valid and Aad_Iv_Encrypted_Icv.Size'Valid)
      then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                           := Request.Ike.Isa_Decrypt.Null_Request;
      Req.Data.Isa_Id               := Isa_Id;
      Req.Data.Aad_Len              := Aad_Len;
      Req.Data.Aad_Iv_Encrypted_Icv := Aad_Iv_Encrypted_Icv;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Isa_Decrypt.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Decrypt.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Decrypted := Res.Data.Decrypted;
      end if;
   end Isa_Decrypt;

   -------------------------------------------------------------------------

   procedure Isa_Encrypt
     (Result           : out Results.Result_Type;
      Isa_Id           :     Types.Isa_Id_Type;
      Aad_Len          :     Types.Aad_Len_Type;
      Aad_Plain        :     Types.Aad_Plain_Type;
      Iv_Encrypted_Icv : out Types.Iv_Encrypted_Icv_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Isa_Encrypt.Request_Type;
      Res  : Response.Ike.Isa_Encrypt.Response_Type;
      Data : Response.Data_Type;
   begin
      Iv_Encrypted_Icv := Types.Null_Iv_Encrypted_Icv_Type;
      if not (Isa_Id'Valid and Aad_Len'Valid and Aad_Plain.Size'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                := Request.Ike.Isa_Encrypt.Null_Request;
      Req.Data.Isa_Id    := Isa_Id;
      Req.Data.Aad_Len   := Aad_Len;
      Req.Data.Aad_Plain := Aad_Plain;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Isa_Encrypt.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Encrypt.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Iv_Encrypted_Icv := Res.Data.Iv_Encrypted_Icv;
      end if;
   end Isa_Encrypt;

   -------------------------------------------------------------------------

   procedure Isa_Int_Auth
     (Result  : out Results.Result_Type;
      Isa_Id  :     Types.Isa_Id_Type;
      Inbound :     Types.Inbound_Flag_Type;
      Data_Id :     Types.Blob_Id_Type)
   is
      Req  : Request.Ike.Isa_Int_Auth.Request_Type;
      Res  : Response.Ike.Isa_Int_Auth.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Isa_Id'Valid and Inbound'Valid and Data_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req              := Request.Ike.Isa_Int_Auth.Null_Request;
      Req.Data.Isa_Id  := Isa_Id;
      Req.Data.Inbound := Inbound;
      Req.Data.Data_Id := Data_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Isa_Int_Auth.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Int_Auth.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Isa_Int_Auth;

   -------------------------------------------------------------------------

   procedure Isa_Reset
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type)
   is
      Req  : Request.Ike.Isa_Reset.Request_Type;
      Res  : Response.Ike.Isa_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Isa_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req             := Request.Ike.Isa_Reset.Null_Request;
      Req.Data.Isa_Id := Isa_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Isa_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Isa_Reset;

   -------------------------------------------------------------------------

   procedure Isa_Sign
     (Result       : out Results.Result_Type;
      Isa_Id       :     Types.Isa_Id_Type;
      Lc_Id        :     Types.Lc_Id_Type;
      Init_Message :     Types.Init_Message_Type;
      Signature    : out Types.Signature_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Isa_Sign.Request_Type;
      Res  : Response.Ike.Isa_Sign.Response_Type;
      Data : Response.Data_Type;
   begin
      Signature := Types.Null_Signature_Type;
      if not (Isa_Id'Valid and Lc_Id'Valid and Init_Message.Size'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                   := Request.Ike.Isa_Sign.Null_Request;
      Req.Data.Isa_Id       := Isa_Id;
      Req.Data.Lc_Id        := Lc_Id;
      Req.Data.Init_Message := Init_Message;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Isa_Sign.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Sign.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Signature := Res.Data.Signature;
      end if;
   end Isa_Sign;

   -------------------------------------------------------------------------

   procedure Isa_Skip_Create_First
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type)
   is
      Req  : Request.Ike.Isa_Skip_Create_First.Request_Type;
      Res  : Response.Ike.Isa_Skip_Create_First.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Isa_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req             := Request.Ike.Isa_Skip_Create_First.Null_Request;
      Req.Data.Isa_Id := Isa_Id;

      Transport.Client.Send_Receive
        (Req_Data =>
           Request.Ike.Isa_Skip_Create_First.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res :=
        Response.Ike.Isa_Skip_Create_First.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Isa_Skip_Create_First;

   -------------------------------------------------------------------------

   procedure Isa_Update
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type;
      Ke_Id  :     Types.Ke_Id_Type)
   is
      Req  : Request.Ike.Isa_Update.Request_Type;
      Res  : Response.Ike.Isa_Update.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Isa_Id'Valid and Ke_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req             := Request.Ike.Isa_Update.Null_Request;
      Req.Data.Isa_Id := Isa_Id;
      Req.Data.Ke_Id  := Ke_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Isa_Update.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Isa_Update.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Isa_Update;

   -------------------------------------------------------------------------

   procedure Ke_Get
     (Result      : out Results.Result_Type;
      Ke_Id       :     Types.Ke_Id_Type;
      Kea_Id      :     Types.Kea_Id_Type;
      Pubvalue_Id :     Types.Blob_Id_Type;
      Ke_Length   : out Types.Blob_Length_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Ke_Get.Request_Type;
      Res  : Response.Ike.Ke_Get.Response_Type;
      Data : Response.Data_Type;
   begin
      Ke_Length := Types.Null_Blob_Length_Type;
      if not (Ke_Id'Valid and Kea_Id'Valid and Pubvalue_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                  := Request.Ike.Ke_Get.Null_Request;
      Req.Data.Ke_Id       := Ke_Id;
      Req.Data.Kea_Id      := Kea_Id;
      Req.Data.Pubvalue_Id := Pubvalue_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Ke_Get.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Ke_Get.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Ke_Length := Res.Data.Ke_Length;
      end if;
   end Ke_Get;

   -------------------------------------------------------------------------

   procedure Ke_Reset
     (Result : out Results.Result_Type;
      Ke_Id  :     Types.Ke_Id_Type)
   is
      Req  : Request.Ike.Ke_Reset.Request_Type;
      Res  : Response.Ike.Ke_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Ke_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req            := Request.Ike.Ke_Reset.Null_Request;
      Req.Data.Ke_Id := Ke_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Ke_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Ke_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Ke_Reset;

   -------------------------------------------------------------------------

   procedure Ke_Set
     (Result      : out Results.Result_Type;
      Ke_Id       :     Types.Ke_Id_Type;
      Kea_Id      :     Types.Kea_Id_Type;
      Pubvalue_Id :     Types.Blob_Id_Type)
   is
      Req  : Request.Ike.Ke_Set.Request_Type;
      Res  : Response.Ike.Ke_Set.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Ke_Id'Valid and Kea_Id'Valid and Pubvalue_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                  := Request.Ike.Ke_Set.Null_Request;
      Req.Data.Ke_Id       := Ke_Id;
      Req.Data.Kea_Id      := Kea_Id;
      Req.Data.Pubvalue_Id := Pubvalue_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Ke_Set.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Ke_Set.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Ke_Set;

   -------------------------------------------------------------------------

   procedure Nc_Create
     (Result       : out Results.Result_Type;
      Nc_Id        :     Types.Nc_Id_Type;
      Nonce_Length :     Types.Nonce_Length_Type;
      Nonce        : out Types.Nonce_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Nc_Create.Request_Type;
      Res  : Response.Ike.Nc_Create.Response_Type;
      Data : Response.Data_Type;
   begin
      Nonce := Types.Null_Nonce_Type;
      if not (Nc_Id'Valid and Nonce_Length'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req                   := Request.Ike.Nc_Create.Null_Request;
      Req.Data.Nc_Id        := Nc_Id;
      Req.Data.Nonce_Length := Nonce_Length;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Nc_Create.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Nc_Create.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Nonce := Res.Data.Nonce;
      end if;
   end Nc_Create;

   -------------------------------------------------------------------------

   procedure Nc_Reset
     (Result : out Results.Result_Type;
      Nc_Id  :     Types.Nc_Id_Type)
   is
      Req  : Request.Ike.Nc_Reset.Request_Type;
      Res  : Response.Ike.Nc_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      if not (Nc_Id'Valid) then
         Result := Results.Invalid_Parameter;
         return;
      end if;

      Req            := Request.Ike.Nc_Reset.Null_Request;
      Req.Data.Nc_Id := Nc_Id;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Nc_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Nc_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Nc_Reset;

   -------------------------------------------------------------------------

   procedure Tkm_Limits
     (Result              : out Results.Result_Type;
      Max_Active_Requests : out Types.Active_Requests_Type;
      Nc_Contexts         : out Types.Nc_Id_Type;
      Ke_Contexts         : out Types.Ke_Id_Type;
      Cc_Contexts         : out Types.Cc_Id_Type;
      Ae_Contexts         : out Types.Ae_Id_Type;
      Isa_Contexts        : out Types.Isa_Id_Type;
      Esa_Contexts        : out Types.Esa_Id_Type;
      Blob_Contexts       : out Types.Blob_Id_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Tkm_Limits.Request_Type;
      Res  : Response.Ike.Tkm_Limits.Response_Type;
      Data : Response.Data_Type;
   begin
      Max_Active_Requests := Types.Null_Active_Requests_Type;
      Nc_Contexts         := Types.Null_Nc_Id_Type;
      Ke_Contexts         := Types.Null_Ke_Id_Type;
      Cc_Contexts         := Types.Null_Cc_Id_Type;
      Ae_Contexts         := Types.Null_Ae_Id_Type;
      Isa_Contexts        := Types.Null_Isa_Id_Type;
      Esa_Contexts        := Types.Null_Esa_Id_Type;
      Blob_Contexts       := Types.Null_Blob_Id_Type;
      Req                 := Request.Ike.Tkm_Limits.Null_Request;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Tkm_Limits.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Tkm_Limits.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Max_Active_Requests := Res.Data.Max_Active_Requests;
         Nc_Contexts         := Res.Data.Nc_Contexts;
         Ke_Contexts         := Res.Data.Ke_Contexts;
         Cc_Contexts         := Res.Data.Cc_Contexts;
         Ae_Contexts         := Res.Data.Ae_Contexts;
         Isa_Contexts        := Res.Data.Isa_Contexts;
         Esa_Contexts        := Res.Data.Esa_Contexts;
         Blob_Contexts       := Res.Data.Blob_Contexts;
      end if;
   end Tkm_Limits;

   -------------------------------------------------------------------------

   procedure Tkm_Reset (Result : out Results.Result_Type) is
      Req  : Request.Ike.Tkm_Reset.Request_Type;
      Res  : Response.Ike.Tkm_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      Req := Request.Ike.Tkm_Reset.Null_Request;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Tkm_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Tkm_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Tkm_Reset;

   -------------------------------------------------------------------------

   procedure Tkm_Version
     (Result  : out Results.Result_Type;
      Version : out Types.Version_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Ike.Tkm_Version.Request_Type;
      Res  : Response.Ike.Tkm_Version.Response_Type;
      Data : Response.Data_Type;
   begin
      Version := Types.Null_Version_Type;
      Req     := Request.Ike.Tkm_Version.Null_Request;

      Transport.Client.Send_Receive
        (Req_Data => Request.Ike.Tkm_Version.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Ike.Tkm_Version.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Version := Res.Data.Version;
      end if;
   end Tkm_Version;

end Tkmrpc.Clients.Ike;
