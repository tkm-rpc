pragma Style_Checks ("-m-r");

with Interfaces.C.Strings;

with Tkmrpc.Types;
with Tkmrpc.Results;

package Tkmrpc.Clients.Ike is

   procedure Init
     (Result  : out Results.Result_Type;
      Address :     Interfaces.C.Strings.Chars_Ptr);
   pragma Export (C, Init, "ike_init");
   pragma Export_Valued_Procedure (Init);
   --  Initialize IKE client with given address.

   procedure Finalize;
   pragma Export (C, Finalize, "ike_finalize");
   --  Finalize IKE client.

   procedure Tkm_Version
     (Result  : out Results.Result_Type;
      Version : out Types.Version_Type);
   pragma Export (C, Tkm_Version, "ike_tkm_version");
   pragma Export_Valued_Procedure (Tkm_Version);
   --  Returns the version of TKM.

   procedure Tkm_Limits
     (Result              : out Results.Result_Type;
      Max_Active_Requests : out Types.Active_Requests_Type;
      Nc_Contexts         : out Types.Nc_Id_Type;
      Ke_Contexts         : out Types.Ke_Id_Type;
      Cc_Contexts         : out Types.Cc_Id_Type;
      Ae_Contexts         : out Types.Ae_Id_Type;
      Isa_Contexts        : out Types.Isa_Id_Type;
      Esa_Contexts        : out Types.Esa_Id_Type;
      Blob_Contexts       : out Types.Blob_Id_Type);
   pragma Export (C, Tkm_Limits, "ike_tkm_limits");
   pragma Export_Valued_Procedure (Tkm_Limits);
   --  Returns limits of fixed length of TKM IKE.

   procedure Tkm_Reset (Result : out Results.Result_Type);
   pragma Export (C, Tkm_Reset, "ike_tkm_reset");
   pragma Export_Valued_Procedure (Tkm_Reset);
   --  Reset the TKM - IKE interface to a known initial state.

   procedure Nc_Reset
     (Result : out Results.Result_Type;
      Nc_Id  :     Types.Nc_Id_Type);
   pragma Export (C, Nc_Reset, "ike_nc_reset");
   pragma Export_Valued_Procedure (Nc_Reset, Mechanism => (Nc_Id => Value));
   --  Reset a NC context.

   procedure Nc_Create
     (Result       : out Results.Result_Type;
      Nc_Id        :     Types.Nc_Id_Type;
      Nonce_Length :     Types.Nonce_Length_Type;
      Nonce        : out Types.Nonce_Type);
   pragma Export (C, Nc_Create, "ike_nc_create");
   pragma Export_Valued_Procedure (Nc_Create,
      Mechanism => (Nc_Id => Value, Nonce_Length => Value));
   --  Create a nonce.

   procedure Ke_Reset
     (Result : out Results.Result_Type;
      Ke_Id  :     Types.Ke_Id_Type);
   pragma Export (C, Ke_Reset, "ike_ke_reset");
   pragma Export_Valued_Procedure (Ke_Reset, Mechanism => (Ke_Id => Value));
   --  Reset a KE context.

   procedure Ke_Get
     (Result      : out Results.Result_Type;
      Ke_Id       :     Types.Ke_Id_Type;
      Kea_Id      :     Types.Kea_Id_Type;
      Pubvalue_Id :     Types.Blob_Id_Type;
      Ke_Length   : out Types.Blob_Length_Type);
   pragma Export (C, Ke_Get, "ike_ke_get");
   pragma Export_Valued_Procedure (Ke_Get,
      Mechanism => (Ke_Id => Value, Kea_Id => Value, Pubvalue_Id => Value));
   --  Return a KE public value.

   procedure Ke_Set
     (Result      : out Results.Result_Type;
      Ke_Id       :     Types.Ke_Id_Type;
      Kea_Id      :     Types.Kea_Id_Type;
      Pubvalue_Id :     Types.Blob_Id_Type);
   pragma Export (C, Ke_Set, "ike_ke_set");
   pragma Export_Valued_Procedure (Ke_Set,
      Mechanism => (Ke_Id => Value, Kea_Id => Value, Pubvalue_Id => Value));
   --  Set the public value/key of the peer.

   procedure Cc_Reset
     (Result : out Results.Result_Type;
      Cc_Id  :     Types.Cc_Id_Type);
   pragma Export (C, Cc_Reset, "ike_cc_reset");
   pragma Export_Valued_Procedure (Cc_Reset, Mechanism => (Cc_Id => Value));
   --  Reset a CC context.

   procedure Cc_Check_Chain
     (Result : out Results.Result_Type;
      Cc_Id  :     Types.Cc_Id_Type;
      Ri_Id  :     Types.Ri_Id_Type);
   pragma Export (C, Cc_Check_Chain, "ike_cc_check_chain");
   pragma Export_Valued_Procedure (Cc_Check_Chain,
      Mechanism => (Cc_Id => Value, Ri_Id => Value));
   --  Check certificate chain of context specified by cc_id.

   procedure Cc_Add_Certificate
     (Result      : out Results.Result_Type;
      Cc_Id       :     Types.Cc_Id_Type;
      Autha_Id    :     Types.Autha_Id_Type;
      Certificate :     Types.Certificate_Type);
   pragma Export (C, Cc_Add_Certificate, "ike_cc_add_certificate");
   pragma Export_Valued_Procedure (Cc_Add_Certificate,
      Mechanism =>
        (Cc_Id => Value, Autha_Id => Value, Certificate => Value));
   --  Add a certificate to a certificate chain.

   procedure Cc_Check_Ca
     (Result         : out Results.Result_Type;
      Cc_Id          :     Types.Cc_Id_Type;
      Ca_Id          :     Types.Ca_Id_Type;
      Ca_Certificate :     Types.Certificate_Type);
   pragma Export (C, Cc_Check_Ca, "ike_cc_check_ca");
   pragma Export_Valued_Procedure (Cc_Check_Ca,
      Mechanism =>
        (Cc_Id => Value, Ca_Id => Value, Ca_Certificate => Value));
   --  Checks that given root CA certificate matches ca_id and link it to the given CC context.

   procedure Ae_Reset
     (Result : out Results.Result_Type;
      Ae_Id  :     Types.Ae_Id_Type);
   pragma Export (C, Ae_Reset, "ike_ae_reset");
   pragma Export_Valued_Procedure (Ae_Reset, Mechanism => (Ae_Id => Value));
   --  Reset an AE context.

   procedure Isa_Reset
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type);
   pragma Export (C, Isa_Reset, "ike_isa_reset");
   pragma Export_Valued_Procedure (Isa_Reset,
      Mechanism => (Isa_Id => Value));
   --  Reset an ISA context.

   procedure Isa_Create
     (Result    : out Results.Result_Type;
      Isa_Id    :     Types.Isa_Id_Type;
      Ae_Id     :     Types.Ae_Id_Type;
      Ia_Id     :     Types.Ia_Id_Type;
      Ke_Id     :     Types.Ke_Id_Type;
      Nc_Loc_Id :     Types.Nc_Id_Type;
      Nonce_Rem :     Types.Nonce_Type;
      Initiator :     Types.Init_Type;
      Spi_Loc   :     Types.Ike_Spi_Type;
      Spi_Rem   :     Types.Ike_Spi_Type;
      Block_Len : out Types.Block_Len_Type;
      Icv_Len   : out Types.Icv_Len_Type;
      Iv_Len    : out Types.Iv_Len_Type);
   pragma Export (C, Isa_Create, "ike_isa_create");
   pragma Export_Valued_Procedure (Isa_Create,
      Mechanism =>
        (Isa_Id    => Value, Ae_Id => Value, Ia_Id => Value, Ke_Id => Value,
         Nc_Loc_Id => Value, Nonce_Rem => Value, Initiator => Value,
         Spi_Loc   => Value, Spi_Rem => Value));
   --  Create an IKE SA context.

   procedure Isa_Sign
     (Result       : out Results.Result_Type;
      Isa_Id       :     Types.Isa_Id_Type;
      Lc_Id        :     Types.Lc_Id_Type;
      Init_Message :     Types.Init_Message_Type;
      Signature    : out Types.Signature_Type);
   pragma Export (C, Isa_Sign, "ike_isa_sign");
   pragma Export_Valued_Procedure (Isa_Sign,
      Mechanism => (Isa_Id => Value, Lc_Id => Value, Init_Message => Value));
   --  Provide authentication to the remote endpoint.

   procedure Isa_Auth
     (Result       : out Results.Result_Type;
      Isa_Id       :     Types.Isa_Id_Type;
      Cc_Id        :     Types.Cc_Id_Type;
      Init_Message :     Types.Init_Message_Type;
      Signature    :     Types.Signature_Type);
   pragma Export (C, Isa_Auth, "ike_isa_auth");
   pragma Export_Valued_Procedure (Isa_Auth,
      Mechanism =>
        (Isa_Id    => Value, Cc_Id => Value, Init_Message => Value,
         Signature => Value));
   --  Authenticate the remote endpoint.

   procedure Isa_Create_Child
     (Result        : out Results.Result_Type;
      Isa_Id        :     Types.Isa_Id_Type;
      Parent_Isa_Id :     Types.Isa_Id_Type;
      Ia_Id         :     Types.Ia_Id_Type;
      Ke_Ids        :     Types.Ke_Ids_Type;
      Nc_Loc_Id     :     Types.Nc_Id_Type;
      Nonce_Rem     :     Types.Nonce_Type;
      Initiator     :     Types.Init_Type;
      Spi_Loc       :     Types.Ike_Spi_Type;
      Spi_Rem       :     Types.Ike_Spi_Type;
      Block_Len     : out Types.Block_Len_Type;
      Icv_Len       : out Types.Icv_Len_Type;
      Iv_Len        : out Types.Iv_Len_Type);
   pragma Export (C, Isa_Create_Child, "ike_isa_create_child");
   pragma Export_Valued_Procedure (Isa_Create_Child,
      Mechanism =>
        (Isa_Id    => Value, Parent_Isa_Id => Value, Ia_Id => Value,
         Ke_Ids    => Value, Nc_Loc_Id => Value, Nonce_Rem => Value,
         Initiator => Value, Spi_Loc => Value, Spi_Rem => Value));
   --  Derive an IKE SA context from an existing SA.

   procedure Isa_Skip_Create_First
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type);
   pragma Export (C, Isa_Skip_Create_First, "ike_isa_skip_create_first");
   pragma Export_Valued_Procedure (Isa_Skip_Create_First,
      Mechanism => (Isa_Id => Value));
   --  Don't create a first child.

   procedure Isa_Encrypt
     (Result           : out Results.Result_Type;
      Isa_Id           :     Types.Isa_Id_Type;
      Aad_Len          :     Types.Aad_Len_Type;
      Aad_Plain        :     Types.Aad_Plain_Type;
      Iv_Encrypted_Icv : out Types.Iv_Encrypted_Icv_Type);
   pragma Export (C, Isa_Encrypt, "ike_isa_encrypt");
   pragma Export_Valued_Procedure (Isa_Encrypt,
      Mechanism => (Isa_Id => Value, Aad_Len => Value, Aad_Plain => Value));
   --  Encrypt IKE traffic.

   procedure Isa_Decrypt
     (Result               : out Results.Result_Type;
      Isa_Id               :     Types.Isa_Id_Type;
      Aad_Len              :     Types.Aad_Len_Type;
      Aad_Iv_Encrypted_Icv :     Types.Aad_Iv_Encrypted_Icv_Type;
      Decrypted            : out Types.Decrypted_Type);
   pragma Export (C, Isa_Decrypt, "ike_isa_decrypt");
   pragma Export_Valued_Procedure (Isa_Decrypt,
      Mechanism =>
        (Isa_Id => Value, Aad_Len => Value, Aad_Iv_Encrypted_Icv => Value));
   --  Decrypt IKE traffic.

   procedure Isa_Update
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type;
      Ke_Id  :     Types.Ke_Id_Type);
   pragma Export (C, Isa_Update, "ike_isa_update");
   pragma Export_Valued_Procedure (Isa_Update,
      Mechanism => (Isa_Id => Value, Ke_Id => Value));
   --  Update IKE SA key material using intermediate key exchange.

   procedure Isa_Int_Auth
     (Result  : out Results.Result_Type;
      Isa_Id  :     Types.Isa_Id_Type;
      Inbound :     Types.Inbound_Flag_Type;
      Data_Id :     Types.Blob_Id_Type);
   pragma Export (C, Isa_Int_Auth, "ike_isa_int_auth");
   pragma Export_Valued_Procedure (Isa_Int_Auth,
      Mechanism => (Isa_Id => Value, Inbound => Value, Data_Id => Value));
   --  Authenticate intermediate key exchange.

   procedure Esa_Reset
     (Result : out Results.Result_Type;
      Esa_Id :     Types.Esa_Id_Type);
   pragma Export (C, Esa_Reset, "ike_esa_reset");
   pragma Export_Valued_Procedure (Esa_Reset,
      Mechanism => (Esa_Id => Value));
   --  Reset an ESA context.

   procedure Esa_Create
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Ke_Ids      :     Types.Ke_Ids_Type;
      Nc_Loc_Id   :     Types.Nc_Id_Type;
      Nonce_Rem   :     Types.Nonce_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type);
   pragma Export (C, Esa_Create, "ike_esa_create");
   pragma Export_Valued_Procedure (Esa_Create,
      Mechanism =>
        (Esa_Id    => Value, Isa_Id => Value, Sp_Id => Value, Ea_Id => Value,
         Ke_Ids    => Value, Nc_Loc_Id => Value, Nonce_Rem => Value,
         Esa_Flags => Value, Esp_Spi_Loc => Value, Esp_Spi_Rem => Value));
   --  Creates an ESP SA.

   procedure Esa_Create_No_Pfs
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Nc_Loc_Id   :     Types.Nc_Id_Type;
      Nonce_Rem   :     Types.Nonce_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type);
   pragma Export (C, Esa_Create_No_Pfs, "ike_esa_create_no_pfs");
   pragma Export_Valued_Procedure (Esa_Create_No_Pfs,
      Mechanism =>
        (Esa_Id => Value, Isa_Id => Value, Sp_Id => Value, Ea_Id => Value,
         Nc_Loc_Id   => Value, Nonce_Rem => Value, Esa_Flags => Value,
         Esp_Spi_Loc => Value, Esp_Spi_Rem => Value));
   --  Creates an ESP SA without PFS.

   procedure Esa_Create_First
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type);
   pragma Export (C, Esa_Create_First, "ike_esa_create_first");
   pragma Export_Valued_Procedure (Esa_Create_First,
      Mechanism =>
        (Esa_Id    => Value, Isa_Id => Value, Sp_Id => Value, Ea_Id => Value,
         Esa_Flags => Value, Esp_Spi_Loc => Value, Esp_Spi_Rem => Value));
   --  Creates the first ESP SA for an AE.

   procedure Esa_Select
     (Result : out Results.Result_Type;
      Esa_Id :     Types.Esa_Id_Type);
   pragma Export (C, Esa_Select, "ike_esa_select");
   pragma Export_Valued_Procedure (Esa_Select,
      Mechanism => (Esa_Id => Value));
   --  Selects an ESA context for outgoing traffic.

   procedure Blob_Reset
     (Result  : out Results.Result_Type;
      Blob_Id :     Types.Blob_Id_Type);
   pragma Export (C, Blob_Reset, "ike_blob_reset");
   pragma Export_Valued_Procedure (Blob_Reset,
      Mechanism => (Blob_Id => Value));
   --  Reset a Blob context.

   procedure Blob_Create
     (Result  : out Results.Result_Type;
      Blob_Id :     Types.Blob_Id_Type;
      Length  :     Types.Blob_Length_Type);
   pragma Export (C, Blob_Create, "ike_blob_create");
   pragma Export_Valued_Procedure (Blob_Create,
      Mechanism => (Blob_Id => Value, Length => Value));
   --  Creates a Blob of a given size.

   procedure Blob_Read
     (Result    : out Results.Result_Type;
      Blob_Id   :     Types.Blob_Id_Type;
      Offset    :     Types.Blob_Offset_Type;
      Length    :     Types.Blob_Length_Type;
      Blob_Data : out Types.Blob_Out_Bytes_Type);
   pragma Export (C, Blob_Read, "ike_blob_read");
   pragma Export_Valued_Procedure (Blob_Read,
      Mechanism => (Blob_Id => Value, Offset => Value, Length => Value));
   --  Reads data from a Blob.

   procedure Blob_Write
     (Result    : out Results.Result_Type;
      Blob_Id   :     Types.Blob_Id_Type;
      Offset    :     Types.Blob_Offset_Type;
      Blob_Data :     Types.Blob_In_Bytes_Type);
   pragma Export (C, Blob_Write, "ike_blob_write");
   pragma Export_Valued_Procedure (Blob_Write,
      Mechanism => (Blob_Id => Value, Offset => Value, Blob_Data => Value));
   --  Writes data to a Blob.

end Tkmrpc.Clients.Ike;
