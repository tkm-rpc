pragma Style_Checks ("-o");

with Tkmrpc.Transport.Client;
with Tkmrpc.Request.Cfg.Tkm_Version.Convert;
with Tkmrpc.Request.Cfg.Tkm_Limits.Convert;
with Tkmrpc.Request.Cfg.Tkm_Reset.Convert;
with Tkmrpc.Response.Cfg.Tkm_Version.Convert;
with Tkmrpc.Response.Cfg.Tkm_Limits.Convert;
with Tkmrpc.Response.Cfg.Tkm_Reset.Convert;

package body Tkmrpc.Clients.Cfg is

   -------------------------------------------------------------------------

   procedure Init
     (Result  : out Results.Result_Type;
      Address :     Interfaces.C.Strings.Chars_Ptr) is separate;

   -------------------------------------------------------------------------

   procedure Finalize is separate;

   -------------------------------------------------------------------------

   procedure Tkm_Limits
     (Result              : out Results.Result_Type;
      Max_Active_Requests : out Types.Active_Requests_Type;
      Authag_Contexts     : out Types.Authag_Id_Type;
      Cag_Contexts        : out Types.Cag_Id_Type;
      Li_Contexts         : out Types.Li_Id_Type;
      Ri_Contexts         : out Types.Ri_Id_Type;
      Iag_Contexts        : out Types.Iag_Id_Type;
      Eag_Contexts        : out Types.Eag_Id_Type;
      Keag_Contexts       : out Types.Keag_Id_Type;
      Sp_Contexts         : out Types.Sp_Id_Type;
      Authp_Contexts      : out Types.Authp_Id_Type;
      Kep_Contexts        : out Types.Kep_Id_Type;
      Autha_Contexts      : out Types.Autha_Id_Type;
      Ca_Contexts         : out Types.Ca_Id_Type;
      Lc_Contexts         : out Types.Lc_Id_Type;
      Ia_Contexts         : out Types.Ia_Id_Type;
      Ea_Contexts         : out Types.Ea_Id_Type;
      Kea_Contexts        : out Types.Kea_Id_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Cfg.Tkm_Limits.Request_Type;
      Res  : Response.Cfg.Tkm_Limits.Response_Type;
      Data : Response.Data_Type;
   begin
      Max_Active_Requests := Types.Null_Active_Requests_Type;
      Authag_Contexts     := Types.Null_Authag_Id_Type;
      Cag_Contexts        := Types.Null_Cag_Id_Type;
      Li_Contexts         := Types.Null_Li_Id_Type;
      Ri_Contexts         := Types.Null_Ri_Id_Type;
      Iag_Contexts        := Types.Null_Iag_Id_Type;
      Eag_Contexts        := Types.Null_Eag_Id_Type;
      Keag_Contexts       := Types.Null_Keag_Id_Type;
      Sp_Contexts         := Types.Null_Sp_Id_Type;
      Authp_Contexts      := Types.Null_Authp_Id_Type;
      Kep_Contexts        := Types.Null_Kep_Id_Type;
      Autha_Contexts      := Types.Null_Autha_Id_Type;
      Ca_Contexts         := Types.Null_Ca_Id_Type;
      Lc_Contexts         := Types.Null_Lc_Id_Type;
      Ia_Contexts         := Types.Null_Ia_Id_Type;
      Ea_Contexts         := Types.Null_Ea_Id_Type;
      Kea_Contexts        := Types.Null_Kea_Id_Type;
      Req                 := Request.Cfg.Tkm_Limits.Null_Request;

      Transport.Client.Send_Receive
        (Req_Data => Request.Cfg.Tkm_Limits.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Cfg.Tkm_Limits.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Max_Active_Requests := Res.Data.Max_Active_Requests;
         Authag_Contexts     := Res.Data.Authag_Contexts;
         Cag_Contexts        := Res.Data.Cag_Contexts;
         Li_Contexts         := Res.Data.Li_Contexts;
         Ri_Contexts         := Res.Data.Ri_Contexts;
         Iag_Contexts        := Res.Data.Iag_Contexts;
         Eag_Contexts        := Res.Data.Eag_Contexts;
         Keag_Contexts       := Res.Data.Keag_Contexts;
         Sp_Contexts         := Res.Data.Sp_Contexts;
         Authp_Contexts      := Res.Data.Authp_Contexts;
         Kep_Contexts        := Res.Data.Kep_Contexts;
         Autha_Contexts      := Res.Data.Autha_Contexts;
         Ca_Contexts         := Res.Data.Ca_Contexts;
         Lc_Contexts         := Res.Data.Lc_Contexts;
         Ia_Contexts         := Res.Data.Ia_Contexts;
         Ea_Contexts         := Res.Data.Ea_Contexts;
         Kea_Contexts        := Res.Data.Kea_Contexts;
      end if;
   end Tkm_Limits;

   -------------------------------------------------------------------------

   procedure Tkm_Reset (Result : out Results.Result_Type) is
      Req  : Request.Cfg.Tkm_Reset.Request_Type;
      Res  : Response.Cfg.Tkm_Reset.Response_Type;
      Data : Response.Data_Type;
   begin
      Req := Request.Cfg.Tkm_Reset.Null_Request;

      Transport.Client.Send_Receive
        (Req_Data => Request.Cfg.Tkm_Reset.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Cfg.Tkm_Reset.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
   end Tkm_Reset;

   -------------------------------------------------------------------------

   procedure Tkm_Version
     (Result  : out Results.Result_Type;
      Version : out Types.Version_Type)
   is
      use type Tkmrpc.Results.Result_Type;

      Req  : Request.Cfg.Tkm_Version.Request_Type;
      Res  : Response.Cfg.Tkm_Version.Response_Type;
      Data : Response.Data_Type;
   begin
      Version := Types.Null_Version_Type;
      Req     := Request.Cfg.Tkm_Version.Null_Request;

      Transport.Client.Send_Receive
        (Req_Data => Request.Cfg.Tkm_Version.Convert.To_Request (S => Req),
         Res_Data => Data);
      Res := Response.Cfg.Tkm_Version.Convert.From_Response (S => Data);

      Result := Res.Header.Result;
      if Result = Results.Ok then
         Version := Res.Data.Version;
      end if;
   end Tkm_Version;

end Tkmrpc.Clients.Cfg;
