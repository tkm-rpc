with Tkmrpc.Types;

package Tkmrpc.Contexts.ke
is

   type ke_State_Type is
      (clean,
       --  Initial clean state.
       invalid,
       --  Error state.
       stale,
       --  KE context is stale.
       initiator,
       --  Waiting for remote public value.
       responder,
       --  Waiting for private value.
       provisioned,
       --  All data required to generate shared secret is present.
       generated
       --  Key Exchange shared secret has been stored and is ready for use.
      );

   function Get_State
     (Id : Types.ke_id_type)
      return ke_State_Type
   with
     Pre => Is_Valid (Id);

   function Is_Valid (Id : Types.ke_id_type) return Boolean;
   --  Returns True if the given id has a valid value.

   function Has_creation_time
     (Id : Types.ke_id_type;
      creation_time : Types.rel_time_type)
      return Boolean
   with
      Pre => Is_Valid (Id);
   --  Returns True if the context specified by id has the given
   --  creation_time value.

   function Has_kea_id
     (Id : Types.ke_id_type;
      kea_id : Types.kea_id_type)
      return Boolean
   with
      Pre => Is_Valid (Id);
   --  Returns True if the context specified by id has the given
   --  kea_id value.

   function Has_key
     (Id : Types.ke_id_type;
      key : Types.ke_key_type)
      return Boolean
   with
      Pre => Is_Valid (Id);
   --  Returns True if the context specified by id has the given
   --  key value.

   function Has_priv
     (Id : Types.ke_id_type;
      priv : Types.ke_priv_type)
      return Boolean
   with
      Pre => Is_Valid (Id);
   --  Returns True if the context specified by id has the given
   --  priv value.

   function Has_pub
     (Id : Types.ke_id_type;
      pub : Types.ke_pubvalue_type)
      return Boolean
   with
      Pre => Is_Valid (Id);
   --  Returns True if the context specified by id has the given
   --  pub value.

   function Has_State
     (Id : Types.ke_id_type;
      State : ke_State_Type)
      return Boolean
   with
      Pre => Is_Valid (Id);
   --  Returns True if the context specified by id has the given
   --  State value.

   procedure consume
     (Id : Types.ke_id_type;
      shared_key : out Types.ke_key_type)
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, generated)),
     Post => Has_State (Id, clean);

   procedure generate
     (Id : Types.ke_id_type;
      shared_key : Types.ke_key_type;
      timestamp : Types.rel_time_type)
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, provisioned)),
     Post => Has_State (Id, generated) and
             Has_key (Id, shared_key) and
             Has_creation_time (Id, timestamp);

   function get_kea_id
     (Id : Types.ke_id_type)
      return Types.kea_id_type
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, initiator) or
            Has_State (Id, responder) or
            Has_State (Id, provisioned) or
            Has_State (Id, generated)),
     Post => Has_kea_id (Id, get_kea_id'Result);

   function get_pubvalue
     (Id : Types.ke_id_type)
      return Types.ke_pubvalue_type
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, responder) or
            Has_State (Id, provisioned)),
     Post => Has_pub (Id, get_pubvalue'Result);

   function get_secvalue
     (Id : Types.ke_id_type)
      return Types.ke_priv_type
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, initiator) or
            Has_State (Id, provisioned)),
     Post => Has_priv (Id, get_secvalue'Result);

   procedure initiate
     (Id : Types.ke_id_type;
      kea_id : Types.kea_id_type;
      secvalue : Types.ke_priv_type)
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, clean)),
     Post => Has_State (Id, initiator) and
             Has_kea_id (Id, kea_id) and
             Has_priv (Id, secvalue);

   procedure invalidate
     (Id : Types.ke_id_type)
   with
     Pre => Is_Valid (Id),
     Post => Has_State (Id, invalid);

   procedure reset
     (Id : Types.ke_id_type)
   with
     Pre => Is_Valid (Id),
     Post => Has_State (Id, clean);

   procedure respond
     (Id : Types.ke_id_type;
      kea_id : Types.kea_id_type;
      pubvalue : Types.ke_pubvalue_type)
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, clean)),
     Post => Has_State (Id, responder) and
             Has_kea_id (Id, kea_id) and
             Has_pub (Id, pubvalue);

   procedure set_priv
     (Id : Types.ke_id_type;
      secvalue : Types.ke_priv_type)
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, responder)),
     Post => Has_State (Id, provisioned) and
             Has_priv (Id, secvalue);

   procedure set_pubvalue
     (Id : Types.ke_id_type;
      pubvalue : Types.ke_pubvalue_type)
   with
     Pre => Is_Valid (Id) and then
           (Has_State (Id, initiator)),
     Post => Has_State (Id, provisioned) and
             Has_pub (Id, pubvalue);

end Tkmrpc.Contexts.ke;
