package body Tkmrpc.Contexts.ke
is

   pragma Warnings
     (Off, "* already use-visible through previous use type clause");
   use type Types.ke_id_type;
   use type Types.kea_id_type;
   use type Types.rel_time_type;
   use type Types.ke_priv_type;
   use type Types.ke_pubvalue_type;
   use type Types.ke_key_type;
   pragma Warnings
     (On, "* already use-visible through previous use type clause");

   type ke_FSM_Type is record
      State : ke_State_Type;
      kea_id : Types.kea_id_type;
      creation_time : Types.rel_time_type;
      priv : Types.ke_priv_type;
      pub : Types.ke_pubvalue_type;
      key : Types.ke_key_type;
   end record;
   --  Key Exchange Context

   Null_ke_FSM : constant ke_FSM_Type
     := ke_FSM_Type'
      (State => clean,
       kea_id => Types.kea_id_type'First,
       creation_time => Types.rel_time_type'First,
       priv => Types.Null_ke_priv_type,
       pub => Types.Null_ke_pubvalue_type,
       key => Types.Null_ke_key_type);

   type Context_Array_Type is
     array (Types.ke_id_type) of ke_FSM_Type;

   Context_Array : Context_Array_Type := Context_Array_Type'
     (others => (Null_ke_FSM));

   -------------------------------------------------------------------------

   function Get_State
     (Id : Types.ke_id_type)
      return ke_State_Type
   is
   begin
      return Context_Array (Id).State;
   end Get_State;

   -------------------------------------------------------------------------

   function Has_creation_time
     (Id : Types.ke_id_type;
      creation_time : Types.rel_time_type)
      return Boolean
   is (Context_Array (Id).creation_time = creation_time);

   -------------------------------------------------------------------------

   function Has_kea_id
     (Id : Types.ke_id_type;
      kea_id : Types.kea_id_type)
      return Boolean
   is (Context_Array (Id).kea_id = kea_id);

   -------------------------------------------------------------------------

   function Has_key
     (Id : Types.ke_id_type;
      key : Types.ke_key_type)
      return Boolean
   is (Context_Array (Id).key = key);

   -------------------------------------------------------------------------

   function Has_priv
     (Id : Types.ke_id_type;
      priv : Types.ke_priv_type)
      return Boolean
   is (Context_Array (Id).priv = priv);

   -------------------------------------------------------------------------

   function Has_pub
     (Id : Types.ke_id_type;
      pub : Types.ke_pubvalue_type)
      return Boolean
   is (Context_Array (Id).pub = pub);

   -------------------------------------------------------------------------

   function Has_State
     (Id : Types.ke_id_type;
      State : ke_State_Type)
      return Boolean
   is (Context_Array (Id).State = State);

   -------------------------------------------------------------------------

   pragma Warnings
     (Off, "condition can only be False if invalid values present");
   function Is_Valid (Id : Types.ke_id_type) return Boolean
   is (Context_Array'First <= Id and Id <= Context_Array'Last);
   pragma Warnings
     (On, "condition can only be False if invalid values present");

   -------------------------------------------------------------------------

   procedure consume
     (Id : Types.ke_id_type;
      shared_key : out Types.ke_key_type)
   is
   begin
      shared_key := Context_Array (Id).key;
      Context_Array (Id).kea_id := Types.kea_id_type'First;
      Context_Array (Id).creation_time := Types.rel_time_type'First;
      Context_Array (Id).priv := Types.Null_ke_priv_type;
      Context_Array (Id).pub := Types.Null_ke_pubvalue_type;
      Context_Array (Id).key := Types.Null_ke_key_type;
      Context_Array (Id).State := clean;
   end consume;

   -------------------------------------------------------------------------

   procedure generate
     (Id : Types.ke_id_type;
      shared_key : Types.ke_key_type;
      timestamp : Types.rel_time_type)
   is
   begin
      Context_Array (Id).key := shared_key;
      Context_Array (Id).creation_time := timestamp;
      Context_Array (Id).priv := Types.Null_ke_priv_type;
      Context_Array (Id).pub := Types.Null_ke_pubvalue_type;
      Context_Array (Id).State := generated;
   end generate;

   -------------------------------------------------------------------------

   function get_kea_id
     (Id : Types.ke_id_type)
      return Types.kea_id_type
   is
   begin
      return Context_Array (Id).kea_id;
   end get_kea_id;

   -------------------------------------------------------------------------

   function get_pubvalue
     (Id : Types.ke_id_type)
      return Types.ke_pubvalue_type
   is
   begin
      return Context_Array (Id).pub;
   end get_pubvalue;

   -------------------------------------------------------------------------

   function get_secvalue
     (Id : Types.ke_id_type)
      return Types.ke_priv_type
   is
   begin
      return Context_Array (Id).priv;
   end get_secvalue;

   -------------------------------------------------------------------------

   procedure initiate
     (Id : Types.ke_id_type;
      kea_id : Types.kea_id_type;
      secvalue : Types.ke_priv_type)
   is
   begin
      Context_Array (Id).kea_id := kea_id;
      Context_Array (Id).priv := secvalue;
      Context_Array (Id).State := initiator;
   end initiate;

   -------------------------------------------------------------------------

   procedure invalidate
     (Id : Types.ke_id_type)
   is
   begin
      Context_Array (Id).State := invalid;
   end invalidate;

   -------------------------------------------------------------------------

   procedure reset
     (Id : Types.ke_id_type)
   is
   begin
      Context_Array (Id).kea_id := Types.kea_id_type'First;
      Context_Array (Id).creation_time := Types.rel_time_type'First;
      Context_Array (Id).priv := Types.Null_ke_priv_type;
      Context_Array (Id).pub := Types.Null_ke_pubvalue_type;
      Context_Array (Id).key := Types.Null_ke_key_type;
      Context_Array (Id).State := clean;
   end reset;

   -------------------------------------------------------------------------

   procedure respond
     (Id : Types.ke_id_type;
      kea_id : Types.kea_id_type;
      pubvalue : Types.ke_pubvalue_type)
   is
   begin
      Context_Array (Id).kea_id := kea_id;
      Context_Array (Id).pub := pubvalue;
      Context_Array (Id).State := responder;
   end respond;

   -------------------------------------------------------------------------

   procedure set_priv
     (Id : Types.ke_id_type;
      secvalue : Types.ke_priv_type)
   is
   begin
      Context_Array (Id).priv := secvalue;
      Context_Array (Id).State := provisioned;
   end set_priv;

   -------------------------------------------------------------------------

   procedure set_pubvalue
     (Id : Types.ke_id_type;
      pubvalue : Types.ke_pubvalue_type)
   is
   begin
      Context_Array (Id).pub := pubvalue;
      Context_Array (Id).State := provisioned;
   end set_pubvalue;

end Tkmrpc.Contexts.ke;
