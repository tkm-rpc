with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Isa_Update.Convert;
with Tkmrpc.Response.Ike.Isa_Update.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Isa_Update is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Isa_Update.Request_Type;
      Specific_Res : Response.Ike.Isa_Update.Response_Type;
   begin
      Specific_Res := Response.Ike.Isa_Update.Null_Response;

      Specific_Req := Request.Ike.Isa_Update.Convert.From_Request (S => Req);

      if Specific_Req.Data.Isa_Id'Valid and Specific_Req.Data.Ke_Id'Valid
      then
         Servers.Ike.Isa_Update
           (Result => Specific_Res.Header.Result,
            Isa_Id => Specific_Req.Data.Isa_Id,
            Ke_Id  => Specific_Req.Data.Ke_Id);

         Res :=
           Response.Ike.Isa_Update.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Isa_Update;
