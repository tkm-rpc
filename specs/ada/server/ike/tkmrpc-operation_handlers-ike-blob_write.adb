with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Blob_Write.Convert;
with Tkmrpc.Response.Ike.Blob_Write.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Blob_Write is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Blob_Write.Request_Type;
      Specific_Res : Response.Ike.Blob_Write.Response_Type;
   begin
      Specific_Res := Response.Ike.Blob_Write.Null_Response;

      Specific_Req := Request.Ike.Blob_Write.Convert.From_Request (S => Req);

      if Specific_Req.Data.Blob_Id'Valid and
        Specific_Req.Data.Offset'Valid and
        Specific_Req.Data.Blob_Data.Size'Valid
      then
         Servers.Ike.Blob_Write
           (Result    => Specific_Res.Header.Result,
            Blob_Id   => Specific_Req.Data.Blob_Id,
            Offset    => Specific_Req.Data.Offset,
            Blob_Data => Specific_Req.Data.Blob_Data);

         Res :=
           Response.Ike.Blob_Write.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Blob_Write;
