with Tkmrpc.Request;
with Tkmrpc.Response;

package Tkmrpc.Operation_Handlers.Ike.Blob_Write is

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type);
   --  Handler for the blob_write operation.

end Tkmrpc.Operation_Handlers.Ike.Blob_Write;
