pragma Style_Checks ("-m");

with Tkmrpc.Types;
with Tkmrpc.Results;

package Tkmrpc.Servers.Ike
   is

   procedure Init;
   --  Initialize IKE server.

   procedure Finalize;
   --  Finalize IKE server.

   procedure Tkm_Version
     (Result  : out Results.Result_Type;
      Version : out Types.Version_Type);
   --  Returns the version of TKM.

   procedure Tkm_Limits
     (Result              : out Results.Result_Type;
      Max_Active_Requests : out Types.Active_Requests_Type;
      Nc_Contexts         : out Types.Nc_Id_Type;
      Ke_Contexts         : out Types.Ke_Id_Type;
      Cc_Contexts         : out Types.Cc_Id_Type;
      Ae_Contexts         : out Types.Ae_Id_Type;
      Isa_Contexts        : out Types.Isa_Id_Type;
      Esa_Contexts        : out Types.Esa_Id_Type;
      Blob_Contexts       : out Types.Blob_Id_Type);
   --  Returns limits of fixed length of TKM IKE.

   procedure Tkm_Reset (Result : out Results.Result_Type);
   --  Reset the TKM - IKE interface to a known initial state.

   procedure Nc_Reset
     (Result : out Results.Result_Type;
      Nc_Id  :     Types.Nc_Id_Type);
   --  Reset a NC context.

   procedure Nc_Create
     (Result       : out Results.Result_Type;
      Nc_Id        :     Types.Nc_Id_Type;
      Nonce_Length :     Types.Nonce_Length_Type;
      Nonce        : out Types.Nonce_Type);
   --  Create a nonce.

   procedure Ke_Reset
     (Result : out Results.Result_Type;
      Ke_Id  :     Types.Ke_Id_Type);
   --  Reset a KE context.

   procedure Ke_Get
     (Result      : out Results.Result_Type;
      Ke_Id       :     Types.Ke_Id_Type;
      Kea_Id      :     Types.Kea_Id_Type;
      Pubvalue_Id :     Types.Blob_Id_Type;
      Ke_Length   : out Types.Blob_Length_Type);
   --  Return a KE public value.

   procedure Ke_Set
     (Result      : out Results.Result_Type;
      Ke_Id       :     Types.Ke_Id_Type;
      Kea_Id      :     Types.Kea_Id_Type;
      Pubvalue_Id :     Types.Blob_Id_Type);
   --  Set the public value/key of the peer.

   procedure Cc_Reset
     (Result : out Results.Result_Type;
      Cc_Id  :     Types.Cc_Id_Type);
   --  Reset a CC context.

   procedure Cc_Check_Chain
     (Result : out Results.Result_Type;
      Cc_Id  :     Types.Cc_Id_Type;
      Ri_Id  :     Types.Ri_Id_Type);
   --  Check certificate chain of context specified by cc_id.

   procedure Cc_Add_Certificate
     (Result      : out Results.Result_Type;
      Cc_Id       :     Types.Cc_Id_Type;
      Autha_Id    :     Types.Autha_Id_Type;
      Certificate :     Types.Certificate_Type);
   --  Add a certificate to a certificate chain.

   procedure Cc_Check_Ca
     (Result         : out Results.Result_Type;
      Cc_Id          :     Types.Cc_Id_Type;
      Ca_Id          :     Types.Ca_Id_Type;
      Ca_Certificate :     Types.Certificate_Type);
   --  Checks that given root CA certificate matches ca_id and link it to the given CC context.

   procedure Ae_Reset
     (Result : out Results.Result_Type;
      Ae_Id  :     Types.Ae_Id_Type);
   --  Reset an AE context.

   procedure Isa_Reset
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type);
   --  Reset an ISA context.

   procedure Isa_Create
     (Result    : out Results.Result_Type;
      Isa_Id    :     Types.Isa_Id_Type;
      Ae_Id     :     Types.Ae_Id_Type;
      Ia_Id     :     Types.Ia_Id_Type;
      Ke_Id     :     Types.Ke_Id_Type;
      Nc_Loc_Id :     Types.Nc_Id_Type;
      Nonce_Rem :     Types.Nonce_Type;
      Initiator :     Types.Init_Type;
      Spi_Loc   :     Types.Ike_Spi_Type;
      Spi_Rem   :     Types.Ike_Spi_Type;
      Block_Len : out Types.Block_Len_Type;
      Icv_Len   : out Types.Icv_Len_Type;
      Iv_Len    : out Types.Iv_Len_Type);
   --  Create an IKE SA context.

   procedure Isa_Sign
     (Result       : out Results.Result_Type;
      Isa_Id       :     Types.Isa_Id_Type;
      Lc_Id        :     Types.Lc_Id_Type;
      Init_Message :     Types.Init_Message_Type;
      Signature    : out Types.Signature_Type);
   --  Provide authentication to the remote endpoint.

   procedure Isa_Auth
     (Result       : out Results.Result_Type;
      Isa_Id       :     Types.Isa_Id_Type;
      Cc_Id        :     Types.Cc_Id_Type;
      Init_Message :     Types.Init_Message_Type;
      Signature    :     Types.Signature_Type);
   --  Authenticate the remote endpoint.

   procedure Isa_Create_Child
     (Result        : out Results.Result_Type;
      Isa_Id        :     Types.Isa_Id_Type;
      Parent_Isa_Id :     Types.Isa_Id_Type;
      Ia_Id         :     Types.Ia_Id_Type;
      Ke_Ids        :     Types.Ke_Ids_Type;
      Nc_Loc_Id     :     Types.Nc_Id_Type;
      Nonce_Rem     :     Types.Nonce_Type;
      Initiator     :     Types.Init_Type;
      Spi_Loc       :     Types.Ike_Spi_Type;
      Spi_Rem       :     Types.Ike_Spi_Type;
      Block_Len     : out Types.Block_Len_Type;
      Icv_Len       : out Types.Icv_Len_Type;
      Iv_Len        : out Types.Iv_Len_Type);
   --  Derive an IKE SA context from an existing SA.

   procedure Isa_Skip_Create_First
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type);
   --  Don't create a first child.

   procedure Isa_Encrypt
     (Result           : out Results.Result_Type;
      Isa_Id           :     Types.Isa_Id_Type;
      Aad_Len          :     Types.Aad_Len_Type;
      Aad_Plain        :     Types.Aad_Plain_Type;
      Iv_Encrypted_Icv : out Types.Iv_Encrypted_Icv_Type);
   --  Encrypt IKE traffic.

   procedure Isa_Decrypt
     (Result               : out Results.Result_Type;
      Isa_Id               :     Types.Isa_Id_Type;
      Aad_Len              :     Types.Aad_Len_Type;
      Aad_Iv_Encrypted_Icv :     Types.Aad_Iv_Encrypted_Icv_Type;
      Decrypted            : out Types.Decrypted_Type);
   --  Decrypt IKE traffic.

   procedure Isa_Update
     (Result : out Results.Result_Type;
      Isa_Id :     Types.Isa_Id_Type;
      Ke_Id  :     Types.Ke_Id_Type);
   --  Update IKE SA key material using intermediate key exchange.

   procedure Isa_Int_Auth
     (Result  : out Results.Result_Type;
      Isa_Id  :     Types.Isa_Id_Type;
      Inbound :     Types.Inbound_Flag_Type;
      Data_Id :     Types.Blob_Id_Type);
   --  Authenticate intermediate key exchange.

   procedure Esa_Reset
     (Result : out Results.Result_Type;
      Esa_Id :     Types.Esa_Id_Type);
   --  Reset an ESA context.

   procedure Esa_Create
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Ke_Ids      :     Types.Ke_Ids_Type;
      Nc_Loc_Id   :     Types.Nc_Id_Type;
      Nonce_Rem   :     Types.Nonce_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type);
   --  Creates an ESP SA.

   procedure Esa_Create_No_Pfs
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Nc_Loc_Id   :     Types.Nc_Id_Type;
      Nonce_Rem   :     Types.Nonce_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type);
   --  Creates an ESP SA without PFS.

   procedure Esa_Create_First
     (Result      : out Results.Result_Type;
      Esa_Id      :     Types.Esa_Id_Type;
      Isa_Id      :     Types.Isa_Id_Type;
      Sp_Id       :     Types.Sp_Id_Type;
      Ea_Id       :     Types.Ea_Id_Type;
      Esa_Flags   :     Types.Esa_Flags_Type;
      Esp_Spi_Loc :     Types.Esp_Spi_Type;
      Esp_Spi_Rem :     Types.Esp_Spi_Type);
   --  Creates the first ESP SA for an AE.

   procedure Esa_Select
     (Result : out Results.Result_Type;
      Esa_Id :     Types.Esa_Id_Type);
   --  Selects an ESA context for outgoing traffic.

   procedure Blob_Reset
     (Result  : out Results.Result_Type;
      Blob_Id :     Types.Blob_Id_Type);
   --  Reset a Blob context.

   procedure Blob_Create
     (Result  : out Results.Result_Type;
      Blob_Id :     Types.Blob_Id_Type;
      Length  :     Types.Blob_Length_Type);
   --  Creates a Blob of a given size.

   procedure Blob_Read
     (Result    : out Results.Result_Type;
      Blob_Id   :     Types.Blob_Id_Type;
      Offset    :     Types.Blob_Offset_Type;
      Length    :     Types.Blob_Length_Type;
      Blob_Data : out Types.Blob_Out_Bytes_Type);
   --  Reads data from a Blob.

   procedure Blob_Write
     (Result    : out Results.Result_Type;
      Blob_Id   :     Types.Blob_Id_Type;
      Offset    :     Types.Blob_Offset_Type;
      Blob_Data :     Types.Blob_In_Bytes_Type);
   --  Writes data to a Blob.

end Tkmrpc.Servers.Ike;
