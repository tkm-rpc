with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Isa_Encrypt.Convert;
with Tkmrpc.Response.Ike.Isa_Encrypt.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Isa_Encrypt is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Isa_Encrypt.Request_Type;
      Specific_Res : Response.Ike.Isa_Encrypt.Response_Type;
   begin
      Specific_Res := Response.Ike.Isa_Encrypt.Null_Response;

      Specific_Req :=
        Request.Ike.Isa_Encrypt.Convert.From_Request (S => Req);

      if Specific_Req.Data.Isa_Id'Valid and
        Specific_Req.Data.Aad_Len'Valid and
        Specific_Req.Data.Aad_Plain.Size'Valid
      then
         Servers.Ike.Isa_Encrypt
           (Result           => Specific_Res.Header.Result,
            Isa_Id           => Specific_Req.Data.Isa_Id,
            Aad_Len          => Specific_Req.Data.Aad_Len,
            Aad_Plain        => Specific_Req.Data.Aad_Plain,
            Iv_Encrypted_Icv => Specific_Res.Data.Iv_Encrypted_Icv);

         Res :=
           Response.Ike.Isa_Encrypt.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Isa_Encrypt;
