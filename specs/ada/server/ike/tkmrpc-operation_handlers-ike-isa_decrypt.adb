with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Isa_Decrypt.Convert;
with Tkmrpc.Response.Ike.Isa_Decrypt.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Isa_Decrypt is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Isa_Decrypt.Request_Type;
      Specific_Res : Response.Ike.Isa_Decrypt.Response_Type;
   begin
      Specific_Res := Response.Ike.Isa_Decrypt.Null_Response;

      Specific_Req :=
        Request.Ike.Isa_Decrypt.Convert.From_Request (S => Req);

      if Specific_Req.Data.Isa_Id'Valid and
        Specific_Req.Data.Aad_Len'Valid and
        Specific_Req.Data.Aad_Iv_Encrypted_Icv.Size'Valid
      then
         Servers.Ike.Isa_Decrypt
           (Result               => Specific_Res.Header.Result,
            Isa_Id               => Specific_Req.Data.Isa_Id,
            Aad_Len              => Specific_Req.Data.Aad_Len,
            Aad_Iv_Encrypted_Icv => Specific_Req.Data.Aad_Iv_Encrypted_Icv,
            Decrypted            => Specific_Res.Data.Decrypted);

         Res :=
           Response.Ike.Isa_Decrypt.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Isa_Decrypt;
