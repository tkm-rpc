with Tkmrpc.Request;
with Tkmrpc.Response;

package Tkmrpc.Operation_Handlers.Ike.Blob_Create is

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type);
   --  Handler for the blob_create operation.

end Tkmrpc.Operation_Handlers.Ike.Blob_Create;
