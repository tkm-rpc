with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Isa_Create_Child.Convert;
with Tkmrpc.Response.Ike.Isa_Create_Child.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Isa_Create_Child is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Isa_Create_Child.Request_Type;
      Specific_Res : Response.Ike.Isa_Create_Child.Response_Type;
   begin
      Specific_Res := Response.Ike.Isa_Create_Child.Null_Response;

      Specific_Req :=
        Request.Ike.Isa_Create_Child.Convert.From_Request (S => Req);

      if Specific_Req.Data.Isa_Id'Valid and
        Specific_Req.Data.Parent_Isa_Id'Valid and
        Specific_Req.Data.Ia_Id'Valid and
        Specific_Req.Data.Ke_Ids.Size'Valid and
        Specific_Req.Data.Nc_Loc_Id'Valid and
        Specific_Req.Data.Nonce_Rem.Size'Valid and
        Specific_Req.Data.Initiator'Valid and
        Specific_Req.Data.Spi_Loc'Valid and Specific_Req.Data.Spi_Rem'Valid
      then
         Servers.Ike.Isa_Create_Child
           (Result        => Specific_Res.Header.Result,
            Isa_Id        => Specific_Req.Data.Isa_Id,
            Parent_Isa_Id => Specific_Req.Data.Parent_Isa_Id,
            Ia_Id         => Specific_Req.Data.Ia_Id,
            Ke_Ids        => Specific_Req.Data.Ke_Ids,
            Nc_Loc_Id     => Specific_Req.Data.Nc_Loc_Id,
            Nonce_Rem     => Specific_Req.Data.Nonce_Rem,
            Initiator     => Specific_Req.Data.Initiator,
            Spi_Loc       => Specific_Req.Data.Spi_Loc,
            Spi_Rem       => Specific_Req.Data.Spi_Rem,
            Block_Len     => Specific_Res.Data.Block_Len,
            Icv_Len       => Specific_Res.Data.Icv_Len,
            Iv_Len        => Specific_Res.Data.Iv_Len);

         Res :=
           Response.Ike.Isa_Create_Child.Convert.To_Response
             (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Isa_Create_Child;
