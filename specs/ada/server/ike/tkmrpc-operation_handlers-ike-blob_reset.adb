with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Blob_Reset.Convert;
with Tkmrpc.Response.Ike.Blob_Reset.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Blob_Reset is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Blob_Reset.Request_Type;
      Specific_Res : Response.Ike.Blob_Reset.Response_Type;
   begin
      Specific_Res := Response.Ike.Blob_Reset.Null_Response;

      Specific_Req := Request.Ike.Blob_Reset.Convert.From_Request (S => Req);

      if Specific_Req.Data.Blob_Id'Valid then
         Servers.Ike.Blob_Reset
           (Result  => Specific_Res.Header.Result,
            Blob_Id => Specific_Req.Data.Blob_Id);

         Res :=
           Response.Ike.Blob_Reset.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Blob_Reset;
