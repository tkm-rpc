with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Ke_Set.Convert;
with Tkmrpc.Response.Ike.Ke_Set.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Ke_Set is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Ke_Set.Request_Type;
      Specific_Res : Response.Ike.Ke_Set.Response_Type;
   begin
      Specific_Res := Response.Ike.Ke_Set.Null_Response;

      Specific_Req := Request.Ike.Ke_Set.Convert.From_Request (S => Req);

      if Specific_Req.Data.Ke_Id'Valid and Specific_Req.Data.Kea_Id'Valid and
        Specific_Req.Data.Pubvalue_Id'Valid
      then
         Servers.Ike.Ke_Set
           (Result      => Specific_Res.Header.Result,
            Ke_Id       => Specific_Req.Data.Ke_Id,
            Kea_Id      => Specific_Req.Data.Kea_Id,
            Pubvalue_Id => Specific_Req.Data.Pubvalue_Id);

         Res := Response.Ike.Ke_Set.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Ke_Set;
