with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Ke_Get.Convert;
with Tkmrpc.Response.Ike.Ke_Get.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Ke_Get is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Ke_Get.Request_Type;
      Specific_Res : Response.Ike.Ke_Get.Response_Type;
   begin
      Specific_Res := Response.Ike.Ke_Get.Null_Response;

      Specific_Req := Request.Ike.Ke_Get.Convert.From_Request (S => Req);

      if Specific_Req.Data.Ke_Id'Valid and Specific_Req.Data.Kea_Id'Valid and
        Specific_Req.Data.Pubvalue_Id'Valid
      then
         Servers.Ike.Ke_Get
           (Result      => Specific_Res.Header.Result,
            Ke_Id       => Specific_Req.Data.Ke_Id,
            Kea_Id      => Specific_Req.Data.Kea_Id,
            Pubvalue_Id => Specific_Req.Data.Pubvalue_Id,
            Ke_Length   => Specific_Res.Data.Ke_Length);

         Res := Response.Ike.Ke_Get.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Ke_Get;
