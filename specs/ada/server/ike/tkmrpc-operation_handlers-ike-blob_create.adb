with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Blob_Create.Convert;
with Tkmrpc.Response.Ike.Blob_Create.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Blob_Create is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Blob_Create.Request_Type;
      Specific_Res : Response.Ike.Blob_Create.Response_Type;
   begin
      Specific_Res := Response.Ike.Blob_Create.Null_Response;

      Specific_Req :=
        Request.Ike.Blob_Create.Convert.From_Request (S => Req);

      if Specific_Req.Data.Blob_Id'Valid and Specific_Req.Data.Length'Valid
      then
         Servers.Ike.Blob_Create
           (Result  => Specific_Res.Header.Result,
            Blob_Id => Specific_Req.Data.Blob_Id,
            Length  => Specific_Req.Data.Length);

         Res :=
           Response.Ike.Blob_Create.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Blob_Create;
