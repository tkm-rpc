with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Cc_Check_Chain.Convert;
with Tkmrpc.Response.Ike.Cc_Check_Chain.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Cc_Check_Chain is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Cc_Check_Chain.Request_Type;
      Specific_Res : Response.Ike.Cc_Check_Chain.Response_Type;
   begin
      Specific_Res := Response.Ike.Cc_Check_Chain.Null_Response;

      Specific_Req :=
        Request.Ike.Cc_Check_Chain.Convert.From_Request (S => Req);

      if Specific_Req.Data.Cc_Id'Valid and Specific_Req.Data.Ri_Id'Valid then
         Servers.Ike.Cc_Check_Chain
           (Result => Specific_Res.Header.Result,
            Cc_Id  => Specific_Req.Data.Cc_Id,
            Ri_Id  => Specific_Req.Data.Ri_Id);

         Res :=
           Response.Ike.Cc_Check_Chain.Convert.To_Response
             (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Cc_Check_Chain;
