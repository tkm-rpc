with Tkmrpc.Request;
with Tkmrpc.Response;

package Tkmrpc.Operation_Handlers.Ike.Blob_Reset is

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type);
   --  Handler for the blob_reset operation.

end Tkmrpc.Operation_Handlers.Ike.Blob_Reset;
