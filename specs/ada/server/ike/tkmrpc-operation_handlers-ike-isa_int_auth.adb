with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Isa_Int_Auth.Convert;
with Tkmrpc.Response.Ike.Isa_Int_Auth.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Isa_Int_Auth is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Isa_Int_Auth.Request_Type;
      Specific_Res : Response.Ike.Isa_Int_Auth.Response_Type;
   begin
      Specific_Res := Response.Ike.Isa_Int_Auth.Null_Response;

      Specific_Req :=
        Request.Ike.Isa_Int_Auth.Convert.From_Request (S => Req);

      if Specific_Req.Data.Isa_Id'Valid and
        Specific_Req.Data.Inbound'Valid and Specific_Req.Data.Data_Id'Valid
      then
         Servers.Ike.Isa_Int_Auth
           (Result  => Specific_Res.Header.Result,
            Isa_Id  => Specific_Req.Data.Isa_Id,
            Inbound => Specific_Req.Data.Inbound,
            Data_Id => Specific_Req.Data.Data_Id);

         Res :=
           Response.Ike.Isa_Int_Auth.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Isa_Int_Auth;
