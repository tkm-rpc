with Tkmrpc.Request;
with Tkmrpc.Response;

package Tkmrpc.Operation_Handlers.Ike.Ke_Set is

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type);
   --  Handler for the ke_set operation.

end Tkmrpc.Operation_Handlers.Ike.Ke_Set;
