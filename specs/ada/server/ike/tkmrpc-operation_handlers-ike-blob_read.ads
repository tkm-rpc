with Tkmrpc.Request;
with Tkmrpc.Response;

package Tkmrpc.Operation_Handlers.Ike.Blob_Read is

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type);
   --  Handler for the blob_read operation.

end Tkmrpc.Operation_Handlers.Ike.Blob_Read;
