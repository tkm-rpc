with Tkmrpc.Request;
with Tkmrpc.Response;

package Tkmrpc.Operation_Handlers.Ike.Cc_Check_Chain is

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type);
   --  Handler for the cc_check_chain operation.

end Tkmrpc.Operation_Handlers.Ike.Cc_Check_Chain;
