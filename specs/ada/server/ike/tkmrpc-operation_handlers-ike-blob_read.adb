with Tkmrpc.Servers.Ike;
with Tkmrpc.Results;
with Tkmrpc.Request.Ike.Blob_Read.Convert;
with Tkmrpc.Response.Ike.Blob_Read.Convert;

package body Tkmrpc.Operation_Handlers.Ike.Blob_Read is

   -------------------------------------------------------------------------

   procedure Handle
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
      Specific_Req : Request.Ike.Blob_Read.Request_Type;
      Specific_Res : Response.Ike.Blob_Read.Response_Type;
   begin
      Specific_Res := Response.Ike.Blob_Read.Null_Response;

      Specific_Req := Request.Ike.Blob_Read.Convert.From_Request (S => Req);

      if Specific_Req.Data.Blob_Id'Valid and
        Specific_Req.Data.Offset'Valid and Specific_Req.Data.Length'Valid
      then
         Servers.Ike.Blob_Read
           (Result    => Specific_Res.Header.Result,
            Blob_Id   => Specific_Req.Data.Blob_Id,
            Offset    => Specific_Req.Data.Offset,
            Length    => Specific_Req.Data.Length,
            Blob_Data => Specific_Res.Data.Blob_Data);

         Res :=
           Response.Ike.Blob_Read.Convert.To_Response (S => Specific_Res);

      else
         Res.Header.Result := Results.Invalid_Parameter;
      end if;
   end Handle;

end Tkmrpc.Operation_Handlers.Ike.Blob_Read;
