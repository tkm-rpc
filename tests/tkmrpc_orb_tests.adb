--
--  Copyright (C) 2020 Tobias Brunner <tobias@codelabs.ch>
--  Copyright (C) 2013 Reto Buerki <reet@codelabs.ch>
--  Copyright (C) 2013 Adrian-Ken Rueegsegger <ken@codelabs.ch>
--  All rights reserved.
--
--  Redistribution and use in source and binary forms, with or without
--  modification, are permitted provided that the following conditions
--  are met:
--  1. Redistributions of source code must retain the above copyright
--     notice, this list of conditions and the following disclaimer.
--  2. Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
--  3. Neither the name of the University nor the names of its contributors
--     may be used to endorse or promote products derived from this software
--     without specific prior written permission.
--
--  THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
--  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
--  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
--  ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
--  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
--  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
--  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
--  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
--  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
--  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
--  SUCH DAMAGE.
--

with Interfaces.C.Strings;

with GNAT.OS_Lib;

with Anet.Util;
with Anet.Sockets.Unix;
with Anet.Receivers.Stream;

with Tkmrpc.Types;
with Tkmrpc.Clients.Ike;
with Tkmrpc.Dispatchers.Ike;
with Tkmrpc.Mock;
with Tkmrpc.Operations.Ike;
with Tkmrpc.Operation_Handlers.Ike.Nc_Create;
with Tkmrpc.Request;
with Tkmrpc.Response;
with Tkmrpc.Results;
with Tkmrpc.Process_Stream;

pragma Elaborate_All (Anet.Receivers.Stream);
pragma Elaborate_All (Tkmrpc.Process_Stream);

package body Tkmrpc_ORB_Tests
is
   use Ahven;
   use Tkmrpc;

   package ICS renames Interfaces.C.Strings;

   package Unix_TCP_Receiver is new Anet.Receivers.Stream
     (Socket_Type       => Anet.Sockets.Unix.TCP_Socket_Type,
      Address_Type      => Anet.Sockets.Unix.Full_Path_Type,
      Accept_Connection => Anet.Sockets.Unix.Accept_Connection);

   procedure Dispatch is new Process_Stream
     (Address_Type => Anet.Sockets.Unix.Full_Path_Type,
      Dispatch     => Tkmrpc.Dispatchers.Ike.Dispatch);

   procedure Dispatch_Reverse
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type);

   procedure Dispatch_Multi is new Process_Stream
     (Address_Type => Anet.Sockets.Unix.Full_Path_Type,
      Dispatch     => Tkmrpc_ORB_Tests.Dispatch_Reverse);

   Socket_Path : constant String := "/tmp/tkm.rpc-";

   -------------------------------------------------------------------------

   procedure C_Test_Client
   is
      use type Tkmrpc.Types.Nc_Id_Type;
      use type Tkmrpc.Types.Nonce_Length_Type;

      Sock     : aliased Anet.Sockets.Unix.TCP_Socket_Type;
      Receiver : Unix_TCP_Receiver.Receiver_Type (S => Sock'Access);
      Args     : GNAT.OS_Lib.Argument_List (1 .. 1);
      Success  : Boolean := False;
      Path     : constant String
        := Socket_Path & Anet.Util.Random_String (Len => 8);
   begin
      Sock.Init;
      Sock.Bind (Path => Anet.Sockets.Unix.Path_Type (Path));

      Receiver.Listen (Callback => Dispatch'Access);

      Args (1) := new String'(Path);
      GNAT.OS_Lib.Spawn (Program_Name => "./client",
                         Args         => Args,
                         Success      => Success);
      GNAT.OS_Lib.Free (X => Args (1));

      Assert (Condition => Success,
              Message   => "Spawning client failed");

      --  The C test client requests a nonce with id 1 and length 128.

      Assert (Condition => Mock.Last_Nonce_Id = 1,
              Message   => "Last nonce id mismatch");
      Assert (Condition => Mock.Last_Nonce_Length = 128,
              Message   => "Last nonce length mismatch");

      Receiver.Stop;
      Mock.Last_Nonce_Id     := Types.Nc_Id_Type'Last;
      Mock.Last_Nonce_Length := 16;

   exception
      when others =>
         Receiver.Stop;
         Mock.Last_Nonce_Id     := Types.Nc_Id_Type'Last;
         Mock.Last_Nonce_Length := 16;
         raise;
   end C_Test_Client;

   -------------------------------------------------------------------------

   procedure Client_Server_ORBs
   is
      use type Tkmrpc.Types.Nonce_Type;
      use type Tkmrpc.Types.Nonce_Length_Type;
      use type Tkmrpc.Types.Nc_Id_Type;
      use type Tkmrpc.Results.Result_Type;

      Sock     : aliased Anet.Sockets.Unix.TCP_Socket_Type;
      Receiver : Unix_TCP_Receiver.Receiver_Type (S => Sock'Access);
      Nonce    : Types.Nonce_Type;
      Result   : Results.Result_Type;
      Path     : constant String
        := Socket_Path & Anet.Util.Random_String (Len => 8);
      Address  : ICS.chars_ptr
        := ICS.New_String (Str => Path);
   begin
      Sock.Init;
      Sock.Bind (Path => Anet.Sockets.Unix.Path_Type (Path));

      Receiver.Listen (Callback => Dispatch'Access);

      Clients.Ike.Init (Result  => Result,
                        Address => Address);
      ICS.Free (Item => Address);

      Assert (Condition => Result = Results.Ok,
              Message   => "IKE init failed");

      Clients.Ike.Nc_Create (Nc_Id        => 23,
                             Nonce_Length => 243,
                             Nonce        => Nonce,
                             Result       => Result);

      Assert (Condition => Result = Results.Ok,
              Message   => "Remote call failed");
      Assert (Condition => Nonce = Mock.Ref_Nonce,
              Message   => "Nonce incorrect");
      Assert (Condition => Mock.Last_Nonce_Id = 23,
              Message   => "Last nonce id mismatch");
      Assert (Condition => Mock.Last_Nonce_Length = 243,
              Message   => "Last nonce length mismatch");

      Clients.Ike.Finalize;
      Receiver.Stop;
      Mock.Last_Nonce_Id     := Types.Nc_Id_Type'Last;
      Mock.Last_Nonce_Length := 16;

   exception
      when others =>
         Clients.Ike.Finalize;
         Receiver.Stop;
         Mock.Last_Nonce_Id     := Types.Nc_Id_Type'Last;
         Mock.Last_Nonce_Length := 16;
         raise;
   end Client_Server_ORBs;

   -------------------------------------------------------------------------

   procedure Client_Server_ORBs_Multi
   is
      use type Tkmrpc.Types.Nonce_Type;
      use type Tkmrpc.Types.Nonce_Length_Type;
      use type Tkmrpc.Types.Nc_Id_Type;
      use type Tkmrpc.Results.Result_Type;

      protected Index_Creator is
         procedure Get (Value : out Integer);
      private
         Current : Integer := 0;
      end Index_Creator;

      protected body Index_Creator is
         procedure Get (Value : out Integer) is
         begin
            Current := Current + 1;
            Value   := Current;
         end Get;
      end Index_Creator;

      task type Sender_Task is
         entry Start;
         entry Get_Order (Value : out Integer);
      end Sender_Task;

      task body Sender_Task is
         Result : Results.Result_Type;
         Nonce  : Types.Nonce_Type;
         Order  : Integer := 0;
      begin
         loop
            select
               accept Start;

               Clients.Ike.Nc_Create (Nc_Id        => 23,
                                      Nonce_Length => 243,
                                      Nonce        => Nonce,
                                      Result       => Result);

               Index_Creator.Get (Value => Order);

               Assert (Condition => Result = Results.Ok,
                       Message   => "Remote call failed");
               Assert (Condition => Nonce = Mock.Ref_Nonce,
                       Message   => "Nonce incorrect");
            or
               accept Get_Order (Value : out Integer)
               do
                  Value := Order;
               end Get_Order;
            or
               terminate;
            end select;
         end loop;
      end Sender_Task;

      Sender   : array (1 .. 2) of Sender_Task;
      Sock     : aliased Anet.Sockets.Unix.TCP_Socket_Type;
      Receiver : Unix_TCP_Receiver.Receiver_Type (S => Sock'Access);
      Result   : Results.Result_Type;
      Path     : constant String
        := Socket_Path & Anet.Util.Random_String (Len => 8);
      Address  : ICS.chars_ptr
        := ICS.New_String (Str => Path);
      O_1, O_2 : Integer;
   begin
      Sock.Init;
      Sock.Bind (Path => Anet.Sockets.Unix.Path_Type (Path));

      Receiver.Listen (Callback => Dispatch_Multi'Access);

      Clients.Ike.Init (Result  => Result,
                        Address => Address);

      ICS.Free (Item => Address);

      Assert (Condition => Result = Results.Ok,
              Message   => "IKE init failed");

      Sender (1).Start;
      --  Wait a bit so the first request is sent
      delay 0.1;
      Sender (2).Start;

      Sender (1).Get_Order (Value => O_1);
      Sender (2).Get_Order (Value => O_2);

      Assert (Condition => O_1 > O_2,
              Message   => "Responses not received in reverse order");
      Assert (Condition => Mock.Last_Nonce_Id = 23,
              Message   => "Last nonce id mismatch");
      Assert (Condition => Mock.Last_Nonce_Length = 243,
              Message   => "Last nonce length mismatch");

      Clients.Ike.Finalize;
      Receiver.Stop;
      Mock.Last_Nonce_Id     := Types.Nc_Id_Type'Last;
      Mock.Last_Nonce_Length := 16;

   exception
      when others =>
         Clients.Ike.Finalize;
         Receiver.Stop;
         Mock.Last_Nonce_Id     := Types.Nc_Id_Type'Last;
         Mock.Last_Nonce_Length := 16;
         raise;
   end Client_Server_ORBs_Multi;

   -------------------------------------------------------------------------

   procedure Dispatch_Reverse
     (Req :     Request.Data_Type;
      Res : out Response.Data_Type)
   is
   begin
      case Req.Header.Operation is
         when Operations.Ike.Nc_Create =>
            Operation_Handlers.Ike.Nc_Create.Handle (Req => Req, Res => Res);
         when others =>
            Res                  := Response.Null_Data;
            Res.Header.Operation := Req.Header.Operation;
      end case;

      --  We can't really reverse the responses because this is a blocking
      --  call.  So instead, we just let it appear as if the responses were
      --  reversed by setting the Request_Id accordingly, which only works if
      --  the two requests are basically the same.
      case Req.Header.Request_Id is
         when 0 => Res.Header.Request_Id := 1;
         when 1 => Res.Header.Request_Id := 0;
         when others =>
            Res.Header.Request_Id := Req.Header.Request_Id;
      end case;

      --  Delay the responses to make sure both requests have been sent and
      --  there is enough time to detect the order.
      delay 0.25;
   end Dispatch_Reverse;

   -------------------------------------------------------------------------

   procedure Initialize (T : in out Testcase)
   is
   begin
      T.Set_Name (Name => "ORB tests");
      T.Add_Test_Routine
        (Routine => Client_Server_ORBs'Access,
         Name    => "Client/server interaction");
      T.Add_Test_Routine
        (Routine => Client_Server_ORBs_Multi'Access,
         Name    => "Client/server interaction multiple requests");
      T.Add_Test_Routine
        (Routine => C_Test_Client'Access,
         Name    => "C test client");
   end Initialize;

end Tkmrpc_ORB_Tests;
